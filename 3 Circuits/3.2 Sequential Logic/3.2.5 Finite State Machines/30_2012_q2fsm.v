// Consider the state diagram shown below.
//
// Exams 2012q2.png
//
// Write complete Verilog code that represents this FSM. Use separate always blocks for the state table and the state flip-flops, as done in lectures. Describe the FSM output, which is called z, using either continuous assignment statement(s) or an always block (at your discretion). Assign any state codes that you wish to use.
// https://hdlbits.01xz.net/wiki/Exams/2012_q2fsm

module top_module (
    input clk,
    input reset,   // Synchronous active-high reset
    input w,
    output z
);

    parameter
        ST_A = 3'd0,
        ST_B = 3'd1,
        ST_C = 3'd2,
        ST_D = 3'd3,
        ST_E = 3'd4,
        ST_F = 3'd5;

    reg [2:0] state;

    always @(posedge clk) begin
        if (reset) begin
            state <= ST_A;
            z <= 1'b0;
        end else begin
            z <= 1'b0;
            case (state)
                ST_A: begin
                    if (w) begin
                        state <= ST_B;
                    end else begin
                        state <= ST_A;
                    end
                end
                ST_B: begin
                    if (w) begin
                        state <= ST_C;
                    end else begin
                        state <= ST_D;
                    end
                end
                ST_C: begin
                    if (w) begin
                        state <= ST_E;
                        z <= 1'b1;
                    end else begin
                        state <= ST_D;
                    end
                end
                ST_D: begin
                    if (w) begin
                        state <= ST_F;
                        z <= 1'b1;
                    end else begin
                        state <= ST_A;
                    end
                end
                ST_E: begin
                    if (w) begin
                        state <= ST_E;
                        z <= 1'b1;
                    end else begin
                        state <= ST_D;
                    end
                end
                ST_F: begin
                    if (w) begin
                        state <= ST_C;
                    end else begin
                        state <= ST_D;
                    end
                end
                default: begin
                    state <= ST_A;
                    z <= 1'b0;
                end
            endcase
        end
    end
endmodule
