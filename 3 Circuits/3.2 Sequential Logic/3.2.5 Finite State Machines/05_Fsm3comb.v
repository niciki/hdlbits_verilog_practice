//The following is the state transition table for a Moore state machine with one input, one output, and four states. Use the following state encoding: A=2'b00, B=2'b01, C=2'b10, D=2'b11.
//
//    Implement only the state transition logic and output logic (the combinational logic portion) for this state machine. Given the current state (state), compute the next_state and output (out) based on the state transition table.
// https://hdlbits.01xz.net/wiki/Fsm3comb

module top_module(
    input in,
    input [1:0] state,
    output [1:0] next_state,
    output out); //

    parameter
        A = 2'd0,
        B = 2'd1,
        C = 2'd2,
        D = 2'd3;

    always @(*) begin    // This is a sequential always block
        case (state)
            A: begin
                out <= 1'b0;
                if (in) begin
                    next_state <= B;
                end else begin
                    next_state <= A;
                end
            end
            B: begin
                out <= 1'b0;
                if (in) begin
                    next_state <= B;
                end else begin
                    next_state <= C;
                end
            end
            C: begin
                out <= 1'b0;
                if (in) begin
                    next_state <= D;
                end else begin
                    next_state <= A;
                end
            end
            D: begin
                out <= 1'b1;
                if (in) begin
                    next_state <= B;
                end else begin
                    next_state <= C;
                end
            end
            default: begin
                next_state <= A;
                out <= 1'b0;
            end
        endcase
    end
endmodule
