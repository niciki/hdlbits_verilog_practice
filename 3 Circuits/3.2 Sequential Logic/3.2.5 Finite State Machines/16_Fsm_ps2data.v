// Now that you have a state machine that will identify three-byte messages in a PS/2 byte stream, add a datapath that will also output the 24-bit (3 byte) message whenever a packet is received (out_bytes[23:16] is the first byte, out_bytes[15:8] is the second byte, etc.).
//
// out_bytes needs to be valid whenever the done signal is asserted. You may output anything at other times (i.e., don't-care).
//
//     For example:
// https://hdlbits.01xz.net/wiki/Fsm_ps2data

module top_module(
    input clk,
    input [7:0] in,
    input reset,    // Synchronous reset
    output [23:0] out_bytes,
    output done); //

    parameter
        D0 = 2'd0,
        D1 = 2'd1,
    	D2 = 2'd2;

    reg [1:0] state;

    always @(posedge clk) begin
        if (reset) begin
            state <= D0;
            out_bytes <= 0;
            done <= 1'b0;
        end else begin
            done <= 1'b0;
            case (state)
                D0: begin
                    if (in[3]) begin
                        state <= D1;
                        out_bytes[23:16] <= in;
                    end
                end
                D1: begin
                    state <= D2;
                    out_bytes[15:8] <= in;
                end
                D2: begin
                    state <= D0;
                    out_bytes[7:0] <= in;
                    done <= 1'b1;
                end
                default: begin
                    state <= D0;
                    out_bytes <= 0;
                    done <= 1'b0;
                end
            endcase
        end
    end
endmodule
