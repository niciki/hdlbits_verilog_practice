// This is a Moore state machine with two states, two inputs, and one output. Implement this state machine.
// This exercise is the same as fsm2s, but using asynchronous reset.
// https://hdlbits.01xz.net/wiki/Fsm2

module top_module(
    input clk,
    input areset,    // Asynchronous reset to OFF
    input j,
    input k,
    output out); //

    parameter
        stOFF = 1'd0,
    	stON = 1'd1;

    reg state;

    always @(posedge clk, posedge areset) begin    // This is a sequential always block
        if (areset) begin
            state <= stOFF;
            out <= 1'b0;
        end else begin
            case (state)
                stOFF: begin
                    if (j) begin
                        state <= stON;
                        out <= 1'b1;
                    end else begin
                        state <= stOFF;
                        out <= 1'b0;
                    end
                end
                stON: begin
                    if (k) begin
                        state <= stOFF;
                        out <= 1'b0;
                    end else begin
                        state <= stON;
                        out <= 1'b1;
                    end
                end
                default: begin
                    state <= stOFF;
                    out <= 1'b0;
                end
            endcase
        end
    end
endmodule
