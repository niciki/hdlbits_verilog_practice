// The game Lemmings involves critters with fairly simple brains. So simple that we are going to model it using a finite state machine.
//
// In the Lemmings' 2D world, Lemmings can be in one of two states: walking left or walking right. It will switch directions if it hits an obstacle. In particular, if a Lemming is bumped on the left, it will walk right. If it's bumped on the right, it will walk left. If it's bumped on both sides at the same time, it will still switch directions.
//
// Implement a Moore state machine with two states, two inputs, and one output that models this behaviour.
// https://hdlbits.01xz.net/wiki/Lemmings1

module top_module(
    input clk,
    input areset,    // Freshly brainwashed Lemmings walk left.
    input bump_left,
    input bump_right,
    output walk_left,
    output walk_right); //

    parameter
        LEFT = 1'b0,
        RIGHT = 1'b1;

    reg state;

    always @(posedge clk, posedge areset) begin
        if (areset) begin
            state = LEFT;
            walk_left <= 1'b1;
            walk_right <= 1'b0;
        end else begin
            case (state)
                LEFT: begin
                    if (bump_left) begin
                        state = RIGHT;
                        walk_left <= 1'b0;
                        walk_right <= 1'b1;
                    end
                end
                RIGHT: begin
                    if (bump_right) begin
                        state = LEFT;
                        walk_left <= 1'b1;
                        walk_right <= 1'b0;
                    end
                end
                default: begin
                    state = LEFT;
                    walk_left <= 1'b1;
                    walk_right <= 1'b0;
                end
            endcase
        end
    end
endmodule
