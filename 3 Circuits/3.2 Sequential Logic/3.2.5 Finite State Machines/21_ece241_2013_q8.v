// Implement a Mealy-type finite state machine that recognizes the sequence "101" on an input signal named x. Your FSM should have an output signal, z, that is asserted to logic-1 when the "101" sequence is detected. Your FSM should also have an active-low asynchronous reset. You may only have 3 states in your state machine. Your FSM should recognize overlapping sequences.
// https://hdlbits.01xz.net/wiki/Exams/ece241_2013_q8

module top_module (
    input clk,
    input aresetn,    // Asynchronous active-low reset
    input x,
    output z );

    parameter
        ST_INIT = 2'd0,
        ST_1 = 2'd1,
        ST_10 = 2'd2;

    reg [1:0] state;

    always @(posedge clk, negedge aresetn) begin
        if (~aresetn) begin
            state <= ST_INIT;
        end else begin
            case (state)
                ST_INIT: begin
                    if (x) begin
                        state <= ST_1;
                    end
                end
                ST_1: begin
                    if (~x) begin
                        state <= ST_10;
                    end
                end
                ST_10: begin
                    if (x) begin
                        state <= ST_1;
                    end else begin
                        state <= ST_INIT;
                    end
                end
                default: begin
                    state <= ST_INIT;
                end
            endcase
        end
    end
    assign z = (state == ST_10) && x;
endmodule
