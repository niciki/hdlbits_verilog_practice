// Given the state-assigned table shown below, implement the finite-state machine. Reset should reset the FSM to state 000.
// Present state
// y[2:0]	Next state Y[2:0]	Output z
// x=0	    x=1
// 000	    000	        001	     0
// 001	    001	        100	     0
// 010	    010	        001	     0
// 011	    001	        010	     1
// 100	    011	        100	     1
// https://hdlbits.01xz.net/wiki/Exams/2014_q3bfsm

module top_module (
    input clk,
    input reset,   // Synchronous reset
    input x,
    output z
);

    parameter
        ST_000 = 3'b000,
        ST_001 = 3'b001,
        ST_010 = 3'b010,
        ST_011 = 3'b011,
        ST_100 = 3'b100;

    reg [2:0] state;

    always @(posedge clk) begin
        if (reset) begin
            state <= ST_000;
            z <= 1'b0;
        end else begin
            case (state)
                ST_000: begin
                    if (x) begin
                        state <= ST_001;
                        z <= 1'b0;
                    end else begin
                        state <= ST_000;
                        z <= 1'b0;
                    end;
                end
                ST_001: begin
                    if (x) begin
                        state <= ST_100;
                        z <= 1'b1;
                    end else begin
                        state <= ST_001;
                        z <= 1'b0;
                    end;
                end
                ST_010: begin
                    if (x) begin
                        state <= ST_001;
                        z <= 1'b0;
                    end else begin
                        state <= ST_010;
                        z <= 1'b0;
                    end;
                end
                ST_011: begin
                    if (x) begin
                        state <= ST_010;
                        z <= 1'b0;
                    end else begin
                        state <= ST_001;
                        z <= 1'b0;
                    end;
                end
                ST_100: begin
                    if (x) begin
                        state <= ST_100;
                        z <= 1'b1;
                    end else begin
                        state <= ST_011;
                        z <= 1'b1;
                    end;
                end
                default: begin
                    state <= ST_000;
                    z <= 1'b0;
                end
            endcase;
        end;
    end
endmodule
