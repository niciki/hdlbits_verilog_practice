// In many (older) serial communications protocols, each data byte is sent along with a start bit and a stop bit, to help the receiver delimit bytes from the stream of bits. One common scheme is to use one start bit (0), 8 data bits, and 1 stop bit (1). The line is also at logic 1 when nothing is being transmitted (idle).
//
// Design a finite state machine that will identify when bytes have been correctly received when given a stream of bits. It needs to identify the start bit, wait for all 8 data bits, then verify that the stop bit was correct. If the stop bit does not appear when expected, the FSM must wait until it finds a stop bit before attempting to receive the next byte.
// https://hdlbits.01xz.net/wiki/Fsm_serial

module top_module(
    input clk,
    input in,
    input reset,    // Synchronous reset
    output done
);

    parameter
        ST = 2'd0,
        DA = 2'd1,
        SP = 2'd2,
        ER = 2'd3;

    reg [1:0] state;
    reg [3:0] data_cnt = 0;

    always @(posedge clk) begin
        if (reset) begin
            state <= ST;
            done <= 1'b0;
            data_cnt <= 0;
        end else begin
            done <= 1'b0;
            case (state)
                ST: begin
                    data_cnt <= 0;
                    if (in == 0) begin
                        state <= DA;
                    end
                end
                DA: begin
                    data_cnt <= data_cnt + 1'd1;
                    if (data_cnt == 4'd7) begin
                        state <= SP;
                        data_cnt <= 0;
                    end
                end
                SP: begin
                    if (in == 1'b1) begin
                        done <= 1'b1;
                        state <= ST;
                    end else begin
                        state <= ER;
                    end
                end
                ER: begin
                    if (in == 1'b1) begin
                        state <= ST;
                    end
                end
                default: begin
                    state <= ST;
                    done <= 1'b0;
                end
            endcase
        end
    end
endmodule
