// You are to design a one-input one-output serial 2's complementer Moore state machine. The input (x) is a series of bits (one per clock cycle) beginning with the least-significant bit of the number, and the output (Z) is the 2's complement of the input. The machine will accept input numbers of arbitrary length. The circuit requires an asynchronous reset. The conversion begins when Reset is released and stops when Reset is asserted.
// https://hdlbits.01xz.net/wiki/Exams/ece241_2014_q5a

module top_module (
    input clk,
    input areset,
    input x,
    output z
);

    parameter
        ST_RST = 1'b0,
        ST_NORMAL = 1'b1;

    reg state;
    reg c = 0; // carry

    always @(posedge clk, posedge areset) begin
        if (areset) begin
            state <= ST_RST;
            c <= 0;
            z <= 1'b0;
        end else begin
            case (state)
                ST_RST: begin
                    z <= ~x ^ 1'b1;
                    c <= ~x;
                    state <= ST_NORMAL;
                end
                ST_NORMAL: begin
                    z <= ~x ^ c;
                    c <= ~x & c;
                end
                default: begin
                    state <= ST_RST;
                    c <= 0;
                    z <= 1'b0;
                end
            endcase
        end
    end
endmodule
