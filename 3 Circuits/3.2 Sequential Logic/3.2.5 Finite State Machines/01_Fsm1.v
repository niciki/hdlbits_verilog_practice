// This is a Moore state machine with two states, one input, and one output. Implement this state machine. Notice that the reset state is B.
// This exercise is the same as fsm1s, but using asynchronous reset.
// https://hdlbits.01xz.net/wiki/Fsm1

module top_module(
    input clk,
    input areset,    // Asynchronous reset to state B
    input in,
    output out);//

    parameter
    	stA = 1'd0,
    	stB = 1'd1;

    reg state;

    always @(posedge clk, posedge areset) begin    // This is a sequential always block
        if (areset) begin
            state <= stB;
            out <= 1'b1;
        end else begin
            case (state)
                stA: begin
                    if (in) begin
                        state <= stA;
                        out <= 1'b0;
                    end else begin
                        state <= stB;
                        out <= 1'b1;
                    end
                end
                stB: begin
                    if (in) begin
                        state <= stB;
                        out <= 1'b1;
                    end else begin
                        state <= stA;
                        out <= 1'b0;
                    end
                end
                default: begin
                    state <= stB;
                    out <= 1'b1;
                end
            endcase
        end
    end
endmodule
