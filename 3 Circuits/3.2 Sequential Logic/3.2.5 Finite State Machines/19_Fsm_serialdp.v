// We want to add parity checking to the serial receiver. Parity checking adds one extra bit after each data byte. We will use odd parity, where the number of 1s in the 9 bits received must be odd. For example, 101001011 satisfies odd parity (there are 5 1s), but 001001011 does not.
//
//Change your FSM and datapath to perform odd parity checking. Assert the done signal only if a byte is correctly received and its parity check passes. Like the serial receiver FSM, this FSM needs to identify the start bit, wait for all 9 (data and parity) bits, then verify that the stop bit was correct. If the stop bit does not appear when expected, the FSM must wait until it finds a stop bit before attempting to receive the next byte.
//
//    You are provided with the following module that can be used to calculate the parity of the input stream (It's a TFF with reset). The intended use is that it should be given the input bit stream, and reset at appropriate times so it counts the number of 1 bits in each byte.
//
//    module parity (
//        input clk,
//        input reset,
//        input in,
//        output reg odd);
//
//        always @(posedge clk)
//            if (reset) odd <= 0;
//            else if (in) odd <= ~odd;
//
//    endmodule
//
//    Note that the serial protocol sends the least significant bit first, and the parity bit after the 8 data bits.
// https://hdlbits.01xz.net/wiki/Fsm_serialdp

module top_module(
    input clk,
    input in,
    input reset,    // Synchronous reset
    output [7:0] out_byte,
    output done
); //

    parameter
        ST = 3'd0,
        DA = 3'd1,
        PA = 3'd2,
        SP = 3'd3,
        ER = 3'd4;

    reg [2:0] state;
    reg [3:0] data_cnt = 0;

    always @(posedge clk) begin
        if (reset) begin
            state <= ST;
            data_cnt <= 0;
            done <= 1'b0;
            out_byte <= 0;
        end else begin
            done <= 1'b0;
            case (state)
                ST: begin
                    data_cnt <= 0;
                    out_byte <= 0;
                    if (in == 0) begin
                        state <= DA;
                    end
                end
                DA: begin
                    data_cnt <= data_cnt + 1'd1;
                    out_byte[data_cnt] <= in;
                    if (data_cnt == 4'd7) begin
                        state <= PA;
                        data_cnt <= 0;
                    end
                end
                PA: begin
                    if (^out_byte == ~in) begin
                        state <= SP;
                    end else begin
                        state <= ER;
                    end
                end
                SP: begin
                    if (in == 1'b1) begin
                        state <= ST;
                        done <= 1'b1;
                    end else begin
                        state <= ER;
                    end
                end
                ER: begin
                    if (in == 1'b1) begin
                        state <= ST;
                    end
                end
                default: begin
                    state <= ST;
                    done <= 1'b0;
                end
            endcase
        end
    end
endmodule
