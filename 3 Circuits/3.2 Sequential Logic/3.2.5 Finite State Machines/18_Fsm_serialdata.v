// Now that you have a finite state machine that can identify when bytes are correctly received in a serial bitstream, add a datapath that will output the correctly-received data byte. out_byte needs to be valid when done is 1, and is don't-care otherwise.
//
// Note that the serial protocol sends the least significant bit first.
// https://hdlbits.01xz.net/wiki/Fsm_serialdata

module top_module(
    input clk,
    input in,
    input reset,    // Synchronous reset
    output [7:0] out_byte,
    output done
); //

    parameter
        ST = 2'd0,
        DA = 2'd1,
        SP = 2'd2,
        ER = 2'd3;

    reg [1:0] state;
    reg [3:0] data_cnt = 0;

    always @(posedge clk) begin
        if (reset) begin
            state <= ST;
            data_cnt <= 0;
            done <= 1'b0;
            out_byte <= 0;
        end else begin
            done <= 1'b0;
            case (state)
                ST: begin
                    data_cnt <= 0;
                    out_byte <= 0;
                    if (in == 0) begin
                        state <= DA;
                    end
                end
                DA: begin
                    data_cnt <= data_cnt + 1'd1;
                    out_byte[data_cnt] <= in;
                    if (data_cnt == 4'd7) begin
                        state <= SP;
                        data_cnt <= 0;
                    end
                end
                SP: begin
                    if (in == 1'b1) begin
                        state <= ST;
                        done <= 1'b1;
                    end else begin
                        state <= ER;
                        out_byte <= 0;
                    end
                end
                ER: begin
                    if (in == 1'b1) begin
                        state <= ST;
                    end
                end
                default: begin
                    state <= ST;
                    done <= 1'b0;
                end
            endcase
        end
    end
endmodule
