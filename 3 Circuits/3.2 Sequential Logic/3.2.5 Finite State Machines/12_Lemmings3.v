// In addition to walking and falling, Lemmings can sometimes be told to do useful things, like dig (it starts digging when dig=1). A Lemming can dig if it is currently walking on ground (ground=1 and not falling), and will continue digging until it reaches the other side (ground=0). At that point, since there is no ground, it will fall (aaah!), then continue walking in its original direction once it hits ground again. As with falling, being bumped while digging has no effect, and being told to dig when falling or when there is no ground is ignored.
//
//     (In other words, a walking Lemming can fall, dig, or switch directions. If more than one of these conditions are satisfied, fall has higher precedence than dig, which has higher precedence than switching directions.)
//
//     Extend your finite state machine to model this behaviour.
// https://hdlbits.01xz.net/wiki/Lemmings3

module top_module(
    input clk,
    input areset,    // Freshly brainwashed Lemmings walk left.
    input bump_left,
    input bump_right,
    input ground,
    input dig,
    output walk_left,
    output walk_right,
    output aaah,
    output digging );

    parameter
        L = 3'd0,
        R = 3'd1,
        FL = 3'd2,
        FR = 3'd3,
        DL = 3'd4,
        DR = 3'd5;

    reg [2:0] state;

    always @(posedge clk, posedge areset) begin
        if (areset) begin
            state = L;
            walk_left <= 1'b1;
            walk_right <= 1'b0;
            aaah <= 1'b0;
            digging <= 1'b0;
        end else begin
            case (state)
                L: begin
                    if (bump_left) begin
                        state = R;
                        walk_left <= 1'b0;
                        walk_right <= 1'b1;
                        aaah <= 1'b0;
                        digging <= 1'b0;
                    end
                    if (ground == 0) begin
                        state = FL;
                        walk_left <= 1'b0;
                        walk_right <= 1'b0;
                        aaah <= 1'b1;
                        digging <= 1'b0;
                    end else if (dig == 1) begin
                        state = DL;
                        walk_left <= 1'b0;
                        walk_right <= 1'b0;
                        aaah <= 1'b0;
                        digging <= 1'b1;
                    end
                end
                R: begin
                    if (bump_right) begin
                        state = L;
                        walk_left <= 1'b1;
                        walk_right <= 1'b0;
                        aaah <= 1'b0;
                        digging <= 1'b0;
                    end
                    if (ground == 0) begin
                        state = FR;
                        walk_left <= 1'b0;
                        walk_right <= 1'b0;
                        aaah <= 1'b1;
                        digging <= 1'b0;
                    end else if (dig == 1) begin
                        state = DR;
                        walk_left <= 1'b0;
                        walk_right <= 1'b0;
                        aaah <= 1'b0;
                        digging <= 1'b1;
                    end
                end
                FL: begin
                    if (ground) begin
                        state = L;
                        walk_left <= 1'b1;
                        walk_right <= 1'b0;
                        aaah <= 1'b0;
                        digging <= 1'b0;
                    end
                end
                FR: begin
                    if (ground) begin
                        state = R;
                        walk_left <= 1'b0;
                        walk_right <= 1'b1;
                        aaah <= 1'b0;
                        digging <= 1'b0;
                    end
                end
                DL: begin
                    if (ground == 0) begin
                        state = FL;
                        walk_left <= 1'b0;
                        walk_right <= 1'b0;
                        aaah <= 1'b1;
                        digging <= 1'b0;
                    end
                end
                DR: begin
                    if (ground == 0) begin
                        state = FR;
                        walk_left <= 1'b0;
                        walk_right <= 1'b0;
                        aaah <= 1'b1;
                        digging <= 1'b0;
                    end
                end
                default: begin
                    state = L;
                    walk_left <= 1'b1;
                    walk_right <= 1'b0;
                    aaah <= 1'b0;
                    digging <= 1'b0;
                end
            endcase
        end
    end
endmodule
