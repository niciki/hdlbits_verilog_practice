// // Synchronous HDLC framing involves decoding a continuous bit stream of data to look for bit patterns that indicate the beginning and end of frames (packets). Seeing exactly 6 consecutive 1s (i.e., 01111110) is a "flag" that indicate frame boundaries. To avoid the data stream from accidentally containing "flags", the sender inserts a zero after every 5 consecutive 1s which the receiver must detect and discard. We also need to signal an error if there are 7 or more consecutive 1s.
//
// Create a finite state machine to recognize these three sequences:
//
// 0111110: Signal a bit needs to be discarded (disc).
// 01111110: Flag the beginning/end of a frame (flag).
// 01111111...: Error (7 or more 1s) (err).
//
// When the FSM is reset, it should be in a state that behaves as though the previous input were 0.
//
// Here are some example sequences that illustrate the desired operation.
//
// Discard 0111110:
// clkindiscflagerra
//
// b
//
//
// Flag 01111110:
// clkindiscflagerra
//
// b
//
//
// Reset behaviour and error 01111111...:
// clkresetindiscflagerracb
//
// d
//
// Implement this state machine.
// https://hdlbits.01xz.net/wiki/Fsm_hdlc

module top_module(
    input clk,
    input reset,    // Synchronous reset
    input in,
    output disc,
    output flag,
    output err);

    parameter
        ST_INIT = 2'd0,
        ST_ONES = 2'd1,
        ST_ERR = 2'd2;

    reg [1:0] state;
    reg [2:0] cnt_ones = 0;

    always @(posedge clk) begin
        if (reset) begin
            state <= ST_ONES;
            cnt_ones <= 0;
            disc <= 1'b0;
            flag <= 1'b0;
            err <= 1'b0;
        end else begin
            disc <= 1'b0;
            flag <= 1'b0;
            err <= 1'b0;
            case (state)
                ST_INIT: begin
                    state <= ST_ONES;
                    if (in) begin
                        cnt_ones <= 3'd1;
                    end else begin
                        cnt_ones <= 0;
                    end
                end
                ST_ONES: begin
                    if (in) begin
                        cnt_ones <= cnt_ones + 1'd1;
                        if (cnt_ones == 3'd6) begin
                            state <= ST_ERR;
                            cnt_ones <= 0;
                            err <= 1'b1;
                        end
                    end else begin
                        cnt_ones <= 0;
                        if (cnt_ones == 3'd6) begin
                            flag <= 1'b1;
                        end else if (cnt_ones == 3'd5) begin
                            disc <= 1'b1;
                        end
                    end
                end
                ST_ERR: begin
                    if (in) begin
                        err <= 1'b1;
                    end else begin
                        state <= ST_ONES;
                    end
                end
                default: begin
                    state <= ST_INIT;
                    cnt_ones <= 0;
                    disc <= 1'b0;
                    flag <= 1'b0;
                    err <= 1'b0;
                end
            endcase
        end;
    end
endmodule
