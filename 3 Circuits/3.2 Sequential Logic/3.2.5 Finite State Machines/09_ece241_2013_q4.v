// Also include an active-high synchronous reset that resets the state machine to a state equivalent to if the water level had been low for a long time (no sensors asserted, and all four outputs asserted).
// https://hdlbits.01xz.net/wiki/Exams/ece241_2013_q4

module top_module (
    input clk,
    input reset,
    input [3:1] s,
    output fr3,
    output fr2,
    output fr1,
    output dfr
);

    reg [3:1] s_last;
    always @(posedge clk) begin
        if (reset) begin
        	s_last <= S_MIN;
        end else begin
        	s_last <= s;
        end
    end

    parameter
        S_MIN = 3'b000,
        S1 = 3'b001,
        S12 = 3'b011,
        S_MAX = 3'b111;

    always @(posedge clk) begin
        if (reset) begin
            fr3 <= 1'b1;
            fr2 <= 1'b1;
            fr1 <= 1'b1;
            dfr <= 1'b1;
        end else begin
            case (s)
                S_MIN: begin
                    fr3 <= 1'b1;
                    fr2 <= 1'b1;
                    fr1 <= 1'b1;
                    dfr <= 1'b1;
                end
                S_MAX: begin
                    fr3 <= 1'b0;
                    fr2 <= 1'b0;
                    fr1 <= 1'b0;
                    dfr <= 1'b0;
                end
                S12: begin
                    if (s_last < s) begin
                        fr3 <= 1'b0;
                        fr2 <= 1'b0;
                        fr1 <= 1'b1;
                        dfr <= 1'b0;
                    end else if (s_last > s) begin
                        fr3 <= 1'b0;
                        fr2 <= 1'b0;
                        fr1 <= 1'b1;
                        dfr <= 1'b1;
                    end
                end
                S1: begin
                    if (s_last < s) begin
                        fr3 <= 1'b0;
                        fr2 <= 1'b1;
                        fr1 <= 1'b1;
                        dfr <= 1'b0;
                    end else if (s_last > s) begin
                        fr3 <= 1'b0;
                        fr2 <= 1'b1;
                        fr1 <= 1'b1;
                        dfr <= 1'b1;
                    end
                end
                default: begin
                    fr3 <= 1'b1;
                    fr2 <= 1'b1;
                    fr1 <= 1'b1;
                    dfr <= 1'b1;
                end
            endcase
        end
    end
endmodule
