// In addition to walking left and right, Lemmings will fall (and presumably go "aaah!") if the ground disappears underneath them.
//
// In addition to walking left and right and changing direction when bumped, when ground=0, the Lemming will fall and say "aaah!". When the ground reappears (ground=1), the Lemming will resume walking in the same direction as before the fall. Being bumped while falling does not affect the walking direction, and being bumped in the same cycle as ground disappears (but not yet falling), or when the ground reappears while still falling, also does not affect the walking direction.
//
// Build a finite state machine that models this behaviour.
// https://hdlbits.01xz.net/wiki/Lemmings2

module top_module(
    input clk,
    input areset,    // Freshly brainwashed Lemmings walk left.
    input bump_left,
    input bump_right,
    input ground,
    output walk_left,
    output walk_right,
    output aaah );

    parameter
        L = 2'd0,
        R = 2'd1,
        FL = 2'd2,
        FR = 2'd3;

    reg [1:0] state;

    always @(posedge clk, posedge areset) begin
        if (areset) begin
            state = L;
            walk_left <= 1'b1;
            walk_right <= 1'b0;
            aaah <= 1'b0;
        end else begin
            case (state)
                L: begin
                    if (bump_left) begin
                        state = R;
                        walk_left <= 1'b0;
                        walk_right <= 1'b1;
                        aaah <= 1'b0;
                    end
                    if (ground == 0) begin
                        state = FL;
                        walk_left <= 1'b0;
                        walk_right <= 1'b0;
                        aaah <= 1'b1;
                    end
                end
                R: begin
                    if (bump_right) begin
                        state = L;
                        walk_left <= 1'b1;
                        walk_right <= 1'b0;
                        aaah <= 1'b0;
                    end
                    if (ground == 0) begin
                        state = FR;
                        walk_left <= 1'b0;
                        walk_right <= 1'b0;
                        aaah <= 1'b1;
                    end
                end
                FL: begin
                    if (ground) begin
                        state = L;
                        walk_left <= 1'b1;
                        walk_right <= 1'b0;
                        aaah <= 1'b0;
                    end
                end
                FR: begin
                    if (ground) begin
                        state = R;
                        walk_left <= 1'b0;
                        walk_right <= 1'b1;
                        aaah <= 1'b0;
                    end
                end
                default: begin
                    state = L;
                    walk_left <= 1'b1;
                    walk_right <= 1'b0;
                    aaah <= 1'b0;
                end
            endcase
        end
    end
endmodule
