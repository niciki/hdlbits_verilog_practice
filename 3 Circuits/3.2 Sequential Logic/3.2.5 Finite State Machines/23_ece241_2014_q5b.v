// The following diagram is a Mealy machine implementation of the 2's complementer. Implement using one-hot encoding.
// https://hdlbits.01xz.net/wiki/Exams/ece241_2014_q5b

module top_module (
    input clk,
    input areset,
    input x,
    output z
);

    parameter
        ST_RST = 0,
        ST_NORMAL = 1;

    reg [1:0] state;
    reg c = 0; // carry

    always @(posedge clk, posedge areset) begin
        if (areset) begin
            state <= 0;
            state[ST_RST] = 1'b1;
            c <= 0;
        end else begin
            if (state[ST_RST]) begin
                c <= ~x;
                state <= 0;
                state[ST_NORMAL] = 1'b1;
            end else if (state[ST_NORMAL]) begin
                c <= ~x & c;
            end else begin
                state <= 0;
                state[ST_RST] = 1'b1;
                c <= 0;
            end
        end
    end

    assign z = (state[ST_RST] & (~x ^ 1'b1)) | (state[ST_NORMAL] & (~x ^ c));
endmodule
