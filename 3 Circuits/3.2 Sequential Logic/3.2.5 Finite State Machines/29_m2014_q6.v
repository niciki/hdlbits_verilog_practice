// Consider the state machine shown below, which has one input w and one output z.
//
// Exams m2014q6.png
//
// Implement the state machine. (This part wasn't on the midterm, but coding up FSMs is good practice).
// https://hdlbits.01xz.net/wiki/Exams/m2014_q6

module top_module (
    input clk,
    input reset,     // synchronous reset
    input w,
    output z);

    parameter
        ST_A = 3'd0,
        ST_B = 3'd1,
        ST_C = 3'd2,
        ST_D = 3'd3,
        ST_E = 3'd4,
        ST_F = 3'd5;

    reg [2:0] state;

    always @(posedge clk) begin
        if (reset) begin
            state <= ST_A;
            z <= 1'b0;
        end else begin
            z <= 1'b0;
            case (state)
                ST_A: begin
                    if (w) begin
                        state <= ST_A;
                    end else begin
                        state <= ST_B;
                    end
                end
                ST_B: begin
                    if (w) begin
                        state <= ST_D;
                    end else begin
                        state <= ST_C;
                    end
                end
                ST_C: begin
                    if (w) begin
                        state <= ST_D;
                    end else begin
                        state <= ST_E;
                        z <= 1'b1;
                    end
                end
                ST_D: begin
                    if (w) begin
                        state <= ST_A;
                    end else begin
                        state <= ST_F;
                        z <= 1'b1;
                    end
                end
                ST_E: begin
                    if (w) begin
                        state <= ST_D;
                    end else begin
                        state <= ST_E;
                        z <= 1'b1;
                    end
                end
                ST_F: begin
                    if (w) begin
                        state <= ST_D;
                    end else begin
                        state <= ST_C;
                    end
                end
                default: begin
                    state <= ST_A;
                    z <= 1'b0;
                end
            endcase
        end
    end
endmodule
