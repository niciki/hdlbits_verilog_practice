// Although Lemmings can walk, fall, and dig, Lemmings aren't invulnerable. If a Lemming falls for too long then hits the ground, it can splatter. In particular, if a Lemming falls for more than 20 clock cycles then hits the ground, it will splatter and cease walking, falling, or digging (all 4 outputs become 0), forever (Or until the FSM gets reset). There is no upper limit on how far a Lemming can fall before hitting the ground. Lemmings only splatter when hitting the ground; they do not splatter in mid-air.
//
// Extend your finite state machine to model this behaviour.
//
// Falling for 20 cycles is survivable:
// https://hdlbits.01xz.net/wiki/Lemmings4

module top_module(
    input clk,
    input areset,    // Freshly brainwashed Lemmings walk left.
    input bump_left,
    input bump_right,
    input ground,
    input dig,
    output walk_left,
    output walk_right,
    output aaah,
    output digging );

    parameter
        L = 3'd0,
        R = 3'd1,
        FL = 3'd2,
        FR = 3'd3,
        DL = 3'd4,
        DR = 3'd5,
        K = 3'd6;

    reg [2:0] state;
    reg [4:0] fall_cnt = 5'd21;

    always @(posedge clk, posedge areset) begin
        if (areset) begin
            fall_cnt <= 5'd21;
        end else begin
            if (ground == 0) begin
                fall_cnt <= fall_cnt - 1'd1;
                if (fall_cnt == 0) begin
                    fall_cnt <= 0;
                end
            end else begin
                fall_cnt <= 5'd21;
            end
        end
    end

    always @(posedge clk, posedge areset) begin
        if (areset) begin
            state = L;
            walk_left <= 1'b1;
            walk_right <= 1'b0;
            aaah <= 1'b0;
            digging <= 1'b0;
        end else begin
            case (state)
                L: begin
                    if (bump_left) begin
                        state = R;
                        walk_left <= 1'b0;
                        walk_right <= 1'b1;
                        aaah <= 1'b0;
                        digging <= 1'b0;
                    end
                    if (ground == 0) begin
                        state = FL;
                        walk_left <= 1'b0;
                        walk_right <= 1'b0;
                        aaah <= 1'b1;
                        digging <= 1'b0;
                    end else if (dig == 1) begin
                        state = DL;
                        walk_left <= 1'b0;
                        walk_right <= 1'b0;
                        aaah <= 1'b0;
                        digging <= 1'b1;
                    end
                end
                R: begin
                    if (bump_right) begin
                        state = L;
                        walk_left <= 1'b1;
                        walk_right <= 1'b0;
                        aaah <= 1'b0;
                        digging <= 1'b0;
                    end
                    if (ground == 0) begin
                        state = FR;
                        walk_left <= 1'b0;
                        walk_right <= 1'b0;
                        aaah <= 1'b1;
                        digging <= 1'b0;
                    end else if (dig == 1) begin
                        state = DR;
                        walk_left <= 1'b0;
                        walk_right <= 1'b0;
                        aaah <= 1'b0;
                        digging <= 1'b1;
                    end
                end
                FL: begin
                    if (ground) begin
                        if (fall_cnt == 0) begin
                            state = K;
                            walk_left <= 1'b0;
                            walk_right <= 1'b0;
                            aaah <= 1'b0;
                            digging <= 1'b0;
                        end else begin
                            state = L;
                            walk_left <= 1'b1;
                            walk_right <= 1'b0;
                            aaah <= 1'b0;
                            digging <= 1'b0;
                        end
                    end
                end
                FR: begin
                    if (ground) begin
                        if (fall_cnt == 0) begin
                            state = K;
                            walk_left <= 1'b0;
                            walk_right <= 1'b0;
                            aaah <= 1'b0;
                            digging <= 1'b0;
                        end else begin
                            state = R;
                            walk_left <= 1'b0;
                            walk_right <= 1'b1;
                            aaah <= 1'b0;
                            digging <= 1'b0;
                        end
                    end
                end
                DL: begin
                    if (ground == 0) begin
                        state = FL;
                        walk_left <= 1'b0;
                        walk_right <= 1'b0;
                        aaah <= 1'b1;
                        digging <= 1'b0;
                    end
                end
                DR: begin
                    if (ground == 0) begin
                        state = FR;
                        walk_left <= 1'b0;
                        walk_right <= 1'b0;
                        aaah <= 1'b1;
                        digging <= 1'b0;
                    end
                end
                K: begin
                    if (ground == 1) begin
                        state = K;
                        walk_left <= 1'b0;
                        walk_right <= 1'b0;
                        aaah <= 1'b0;
                        digging <= 1'b0;
                    end
                end
                default: begin
                    state = L;
                    walk_left <= 1'b1;
                    walk_right <= 1'b0;
                    aaah <= 1'b0;
                    digging <= 1'b0;
                end
            endcase
        end
    end
endmodule
