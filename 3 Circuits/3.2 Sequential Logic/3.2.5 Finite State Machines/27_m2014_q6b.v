// Consider the state machine shown below, which has one input w and one output z.
//
// Exams m2014q6.png
//
// Assume that you wish to implement the FSM using three flip-flops and state codes y[3:1] = 000, 001, ... , 101 for states A, B, ... , F, respectively. Show a state-assigned table for this FSM. Derive a next-state expression for the flip-flop y[2].
//
// Implement just the next-state logic for y[2]. (This is much more a FSM question than a Verilog coding question. Oh well.)
// https://hdlbits.01xz.net/wiki/Exams/m2014_q6b

module top_module (
    input [3:1] y,
    input w,
    output Y2);

    assign Y2 = (y==3'b001) || (y==3'b010 & w) || (y==3'b100 & w) || (y==3'b101);
endmodule
