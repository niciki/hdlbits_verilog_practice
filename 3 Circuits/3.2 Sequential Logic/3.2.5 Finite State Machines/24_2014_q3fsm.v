// Consider a finite state machine with inputs s and w. Assume that the FSM begins in a reset state called A, as depicted below. The FSM remains in state A as long as s = 0, and it moves to state B when s = 1. Once in state B the FSM examines the value of the input w in the next three clock cycles. If w = 1 in exactly two of these clock cycles, then the FSM has to set an output z to 1 in the following clock cycle. Otherwise z has to be 0. The FSM continues checking w for the next three clock cycles, and so on. The timing diagram below illustrates the required values of z for different values of w.
//
// Use as few states as possible. Note that the s input is used only in state A, so you need to consider just the w input.
// https://hdlbits.01xz.net/wiki/Exams/2014_q3fsm

module top_module (
    input clk,
    input reset,   // Synchronous reset
    input s,
    input w,
    output z
);

    reg [1:0] cnt;
    reg [1:0] ones;

    always @(posedge clk) begin
        if (reset) begin
            cnt <= 0;
            ones <= 0;
            z <= 1'b0;
        end else begin
            z <= 1'b0;
            if (cnt == 2'd0) begin
                if (s) begin
                    cnt <= 2'd1;
                    ones <= 0;
                end;
            end else begin
                cnt <= cnt + 1'd1;
                if (w) begin
                    ones <= ones + 1'd1;
                end
                if (cnt == 2'd3) begin
                    cnt <= 2'd1;
                    ones <= 0;
                    if (((ones == 2'd1) && w) || ((ones == 2'd2) && ~w)) begin
                        z <= 1'b1;
                    end
                end
            end
        end
    end
endmodule
