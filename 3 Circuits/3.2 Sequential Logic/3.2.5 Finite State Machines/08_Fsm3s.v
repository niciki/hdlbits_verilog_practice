// The following is the state transition table for a Moore state machine with one input, one output, and four states. Implement this state machine. Include a synchronous reset that resets the FSM to state A. (This is the same problem as Fsm3 but with a synchronous reset.)
// https://hdlbits.01xz.net/wiki/Fsm3s

module top_module(
    input clk,
    input in,
    input reset,
    output out); //

    parameter
        A = 2'd0,
    	B = 2'd1,
    	C = 2'd2,
    	D = 2'd3;

    reg [1:0] state;

    always @(posedge clk) begin
        if (reset) begin
            state <= A;
            out <= 1'b0;
        end else begin
            case (state)
                A: begin
                    if (in) begin
                        state <= B;
                        out <= 1'b0;
                    end else begin
                        state <= A;
                        out <= 1'b0;
                    end
                end
                B: begin
                    if (in) begin
                        state <= B;
                        out <= 1'b0;
                    end else begin
                        state <= C;
                        out <= 1'b0;
                    end
                end
                C: begin
                    if (in) begin
                        state <= D;
                        out <= 1'b1;
                    end else begin
                        state <= A;
                        out <= 1'b0;
                    end
                end
                D: begin
                    if (in) begin
                        state <= B;
                        out <= 1'b0;
                    end else begin
                        state <= C;
                        out <= 1'b0;
                    end
                end
                default: begin
                    state <= A;
                    out <= 1'b0;
                end
            endcase
        end
    end
endmodule
