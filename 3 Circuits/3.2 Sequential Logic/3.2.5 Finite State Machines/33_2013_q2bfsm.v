// Consider a finite state machine that is used to control some type of motor. The FSM has inputs x and y, which come from the motor, and produces outputs f and g, which control the motor. There is also a clock input called clk and a reset input called resetn.
//
// The FSM has to work as follows. As long as the reset input is asserted, the FSM stays in a beginning state, called state A. When the reset signal is de-asserted, then after the next clock edge the FSM has to set the output f to 1 for one clock cycle. Then, the FSM has to monitor the x input. When x has produced the values 1, 0, 1 in three successive clock cycles, then g should be set to 1 on the following clock cycle. While maintaining g = 1 the FSM has to monitor the y input. If y has the value 1 within at most two clock cycles, then the FSM should maintain g = 1 permanently (that is, until reset). But if y does not become 1 within two clock cycles, then the FSM should set g = 0 permanently (until reset).
//
//     (The original exam question asked for a state diagram only. But here, implement the FSM.)
// https://hdlbits.01xz.net/wiki/Exams/2013_q2bfsm

module top_module (
    input clk,
    input resetn,    // active-low synchronous reset
    input x,
    input y,
    output f,
    output g
);

    parameter
        ST_A = 4'd0,
        ST_WAIT = 4'd1,
        ST_WATCH_X = 4'd2,
        ST_WATCH_X1 = 4'd3,
        ST_WATCH_X10 = 4'd4,
        ST_WATCH_Y = 4'd5,
        ST_WATCH_Y_LAST = 4'd6,
        ST_G0 = 4'd7,
        ST_G1 = 4'd8;

    reg [3:0] state;

    always @(posedge clk) begin
        if (~resetn) begin
            state <= ST_A;
            f <= 1'b0;
            g <= 1'b0;
        end else begin
            f <= 1'b0;
            g <= 1'b0;
            case (state)
                ST_A: begin
                    f <= 1'b1;
                    state <= ST_WAIT;
                end
                ST_WAIT: begin
                    state <= ST_WATCH_X;
                end
                ST_WATCH_X: begin
                    if (x) begin
                        state <= ST_WATCH_X1;
                    end else begin
                        state <= ST_WATCH_X;
                    end
                end
                ST_WATCH_X1: begin
                    if (x) begin
                        state <= ST_WATCH_X1;
                    end else begin
                        state <= ST_WATCH_X10;
                    end
                end
                ST_WATCH_X10: begin
                    if (x) begin
                        state <= ST_WATCH_Y;
                        g <= 1'b1;
                    end else begin
                        state <= ST_WATCH_X;
                    end
                end
                ST_WATCH_Y: begin
                    g <= 1'b1;
                    if (y) begin
                        state <= ST_G1;
                    end else begin
                        state <= ST_WATCH_Y_LAST;
                    end
                end
                ST_WATCH_Y_LAST: begin
                    if (y) begin
                        state <= ST_G1;
                        g <= 1'b1;
                    end else begin
                        state <= ST_G0;
                    end
                end
                ST_G0: begin
                    state <= ST_G0;
                    g <= 1'b0;
                end
                ST_G1: begin
                    state <= ST_G1;
                    g <= 1'b1;
                end
                default: begin
                    state <= ST_A;
                end
            endcase
        end
    end
endmodule
