// The PS/2 mouse protocol sends messages that are three bytes long. However, within a continuous byte stream, it's not obvious where messages start and end. The only indication is that the first byte of each three byte message always has bit[3]=1 (but bit[3] of the other two bytes may be 1 or 0 depending on data).
//
// We want a finite state machine that will search for message boundaries when given an input byte stream. The algorithm we'll use is to discard bytes until we see one with bit[3]=1. We then assume that this is byte 1 of a message, and signal the receipt of a message once all 3 bytes have been received (done).
//
//     The FSM should signal done in the cycle immediately after the third byte of each message was successfully received.

module top_module(
    input clk,
    input [7:0] in,
    input reset,    // Synchronous reset
    output done); //

    parameter
        D0 = 2'd0,
        D1 = 2'd1,
    	D2 = 2'd2;

    reg [1:0] state;

    always @(posedge clk) begin
        if (reset) begin
            state <= D0;
            done <= 1'b0;
        end else begin
            done <= 1'b0;
            case (state)
                D0: begin
                    if (in[3]) begin
                        state <= D1;
                    end
                end
                D1: begin
                    state <= D2;
                end
                D2: begin
                    done <= 1'b1;
                    state <= D0;
                end
                default: begin
                    state <= D0;
                    done <= 1'b0;
                end
            endcase
        end
    end
endmodule
