// Consider the FSM described by the state diagram shown below:
//
// Exams 2013q2.png
//
// This FSM acts as an arbiter circuit, which controls access to some type of resource by three requesting devices. Each device makes its request for the resource by setting a signal r[i] = 1, where r[i] is either r[1], r[2], or r[3]. Each r[i] is an input signal to the FSM, and represents one of the three devices. The FSM stays in state A as long as there are no requests. When one or more request occurs, then the FSM decides which device receives a grant to use the resource and changes to a state that sets that device’s g[i] signal to 1. Each g[i] is an output from the FSM. There is a priority system, in that device 1 has a higher priority than device 2, and device 3 has the lowest priority. Hence, for example, device 3 will only receive a grant if it is the only device making a request when the FSM is in state A. Once a device, i, is given a grant by the FSM, that device continues to receive the grant as long as its request, r[i] = 1.
//
// Write complete Verilog code that represents this FSM. Use separate always blocks for the state table and the state flip-flops, as done in lectures. Describe the FSM outputs, g[i], using either continuous assignment statement(s) or an always block (at your discretion). Assign any state codes that you wish to use.
// https://hdlbits.01xz.net/wiki/Exams/2013_q2afsm

module top_module (
    input clk,
    input resetn,    // active-low synchronous reset
    input [3:1] r,   // request
    output [3:1] g   // grant
);

    parameter
        ST_A = 2'd0,
        ST_B = 2'd1,
        ST_C = 2'd2,
        ST_D = 2'd3;

    reg [1:0] state;

    always @(posedge clk) begin
        if (~resetn) begin
            state <= ST_A;
            g <= 0;
        end else begin
            g <= 0;
            case (state)
                ST_A: begin
                    if (r[1]) begin
                        state <= ST_B;
                        g[1] = 1'b1;
                    end else if (~r[1]&r[2]) begin
                        state <= ST_C;
                        g[2] = 1'b1;
                    end else if (~r[1]&~r[2]&r[3]) begin
                        state <= ST_D;
                        g[3] = 1'b1;
                    end else begin
                        state <= ST_A;
                    end
                end
                ST_B: begin
                    if (r[1]) begin
                        state <= ST_B;
                        g[1] = 1'b1;
                    end else begin
                        state <= ST_A;
                    end
                end
                ST_C: begin
                    if (r[2]) begin
                        state <= ST_C;
                        g[2] = 1'b1;
                    end else begin
                        state <= ST_A;
                    end
                end
                ST_D: begin
                    if (r[3]) begin
                        state <= ST_D;
                        g[3] = 1'b1;
                    end else begin
                        state <= ST_A;
                    end
                end
                default: begin
                    state <= ST_A;
                    g <= 0;
                end
            endcase
        end;
    end
endmodule
