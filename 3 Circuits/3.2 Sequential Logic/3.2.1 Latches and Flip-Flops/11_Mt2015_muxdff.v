// Consider the sequential circuit below:
// Assume that you want to implement hierarchical Verilog code for this circuit, using three instantiations of a submodule that has a flip-flop and multiplexer in it. Write a Verilog module (containing one flip-flop and multiplexer) named top_module for this submodule.
// https://hdlbits.01xz.net/wiki/Mt2015_muxdff

module top_module (
	input clk,
	input L,
	input r_in,
	input q_in,
	output reg Q);

    always @(posedge clk) begin
        Q <= (L) ? r_in : q_in;
    end
endmodule
