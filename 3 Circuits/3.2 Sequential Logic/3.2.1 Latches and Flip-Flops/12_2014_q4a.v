// Consider the n-bit shift register circuit shown below:
// Write a Verilog module named top_module for one stage of this circuit, including both the flip-flop and multiplexers.
// https://hdlbits.01xz.net/wiki/Exams/2014_q4a

module top_module (
    input clk,
    input w, R, E, L,
    output Q
);

    wire tmp;
    assign tmp = (E) ? w : Q;
    always @(posedge clk) begin
        Q <= (L) ? R : tmp;
    end
endmodule
