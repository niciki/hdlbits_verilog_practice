// Given the finite state machine circuit as shown, assume that the D flip-flops are initially reset to zero before the machine begins.
// Build this circuit.
// https://hdlbits.01xz.net/wiki/Exams/ece241_2014_q4

module dff_nq (
    input clk,
    input d,
    input s,
    input c,
    output q,
    output nq
);
    assign nq = ~q;
    always @(posedge clk) begin
        if (c) begin
            q <= 0;
        end else if (s) begin
            q <= 1'b1;
        end else begin
            q <= d;
        end
    end
endmodule

module top_module (
    input clk,
    input x,
    output z
);

    wire dff_nq0_d;
    wire dff_nq0_q;
    dff_nq dff_nq0 (
        .clk(clk),
        .d(dff_nq0_d),
        .s(1'b0),
        .c(1'b0),
        .q(dff_nq0_q),
        .nq()
    );
    assign dff_nq0_d = x ^ dff_nq0_q;

    wire dff_nq1_d;
    wire dff_nq1_q;
    wire dff_nq1_nq;
    dff_nq dff_nq1 (
        .clk(clk),
        .d(dff_nq1_d),
        .s(1'b0),
        .c(1'b0),
        .q(dff_nq1_q),
        .nq(dff_nq1_nq)
    );
    assign dff_nq1_d = x & dff_nq1_nq;

    wire dff_nq2_d;
    wire dff_nq2_q;
    wire dff_nq2_nq;
    dff_nq dff_nq2 (
        .clk(clk),
        .d(dff_nq2_d),
        .s(1'b0),
        .c(1'b0),
        .q(dff_nq2_q),
        .nq(dff_nq2_nq)
    );
    assign dff_nq2_d = x | dff_nq2_nq;

    assign z = ~|{dff_nq0_q, dff_nq1_q, dff_nq2_q};
endmodule
