// Implement the following circuit:
// Note that this is a latch, so a Quartus warning about having inferred a latch is expected.
// https://hdlbits.01xz.net/wiki/Exams/m2014_q4a

module top_module (
    input d,
    input ena,
    output q);

    assign q = (ena) ? d : q;
endmodule
