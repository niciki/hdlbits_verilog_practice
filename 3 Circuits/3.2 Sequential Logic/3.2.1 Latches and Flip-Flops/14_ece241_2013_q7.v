// A JK flip-flop has the below truth table. Implement a JK flip-flop with only a D-type flip-flop and gates. Note: Qold is the output of the D flip-flop before the positive clock edge.
// https://hdlbits.01xz.net/wiki/Exams/ece241_2013_q7

module top_module (
    input clk,
    input j,
    input k,
    output Q);

    wire [1:0] jk;
    assign jk = {j, k};
    always @(posedge clk) begin
        case (jk)
            2'd0: Q <= Q;
            2'd1: Q <= 1'b0;
            2'd2: Q <= 1'b1;
            2'd3: Q <= ~Q;
            default: Q <= Q;
        endcase
    end
endmodule
