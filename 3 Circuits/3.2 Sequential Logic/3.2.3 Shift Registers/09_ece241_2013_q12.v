// In this question, you will design a circuit for an 8x1 memory, where writing to the memory is accomplished by shifting-in bits, and reading is "random access", as in a typical RAM. You will then use the circuit to realize a 3-input logic function.
//
// First, create an 8-bit shift register with 8 D-type flip-flops. Label the flip-flop outputs from Q[0]...Q[7]. The shift register input should be called S, which feeds the input of Q[0] (MSB is shifted in first). The enable input controls whether to shift. Then, extend the circuit to have 3 additional inputs A,B,C and an output Z. The circuit's behaviour should be as follows: when ABC is 000, Z=Q[0], when ABC is 001, Z=Q[1], and so on. Your circuit should contain ONLY the 8-bit shift register, and multiplexers. (Aside: this circuit is called a 3-input look-up-table (LUT)).
// https://hdlbits.01xz.net/wiki/Exams/ece241_2013_q12

module top_module (
    input clk,
    input enable,
    input S,
    input A, B, C,
    output Z );

    reg [7:0] q;
    always @(posedge clk) begin
        if (enable) begin
            q[7:1] <= q[6:0];
            q[0] <= S;
        end
    end

    wire [2:0] abc;
    assign abc = {A, B, C};
    always @(*) begin
        case (abc)
            3'd0: Z = q[0];
            3'd1: Z = q[1];
            3'd2: Z = q[2];
            3'd3: Z = q[3];
            3'd4: Z = q[4];
            3'd5: Z = q[5];
            3'd6: Z = q[6];
            3'd7: Z = q[7];
            default: Z = q[0];
        endcase;
    end
endmodule
