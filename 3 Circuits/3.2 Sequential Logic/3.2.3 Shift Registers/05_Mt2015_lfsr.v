// Taken from 2015 midterm question 5. See also the first part of this question: mt2015_muxdff
//
// Mt2015 muxdff.png
//
// Write the Verilog code for this sequential circuit (Submodules are ok, but the top-level must be named top_module). Assume that you are going to implement the circuit on the DE1-SoC board. Connect the R inputs to the SW switches, connect Clock to KEY[0], and L to KEY[1]. Connect the Q outputs to the red lights LEDR.
// https://hdlbits.01xz.net/wiki/Mt2015_lfsr

module sw_reg (
    input clk,
    input in1,
    input in2,
    input s,
    output q
);

    wire d;
    assign d = (s) ? in1 : in2;

    always @(posedge clk) begin
        q <= d;
    end
endmodule

module top_module (
	input [2:0] SW,      // R
	input [1:0] KEY,     // L and clk
	output [2:0] LEDR);  // Q

    wire [2:0] Q;

    sw_reg sw_reg1 (
        .clk(KEY[0]),
        .in1(SW[0]),
        .in2(Q[2]),
        .s(KEY[1]),
        .q(Q[0])
    );

    sw_reg sw_reg2 (
        .clk(KEY[0]),
        .in1(SW[1]),
        .in2(Q[0]),
        .s(KEY[1]),
        .q(Q[1])
    );

    sw_reg sw_reg3 (
        .clk(KEY[0]),
        .in1(SW[2]),
        .in2(Q[1] ^ Q[2]),
        .s(KEY[1]),
        .q(Q[2])
    );

    assign LEDR = Q;
endmodule
