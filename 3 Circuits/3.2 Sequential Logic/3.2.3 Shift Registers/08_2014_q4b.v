// Consider the n-bit shift register circuit shown below:
//
// Exams 2014q4.png
//
// Write a top-level Verilog module (named top_module) for the shift register, assuming that n = 4. Instantiate four copies of your MUXDFF subcircuit in your top-level module. Assume that you are going to implement the circuit on the DE2 board.
//
//     Connect the R inputs to the SW switches,
//     clk to KEY[0],
//     E to KEY[1],
//     L to KEY[2], and
//     w to KEY[3].
//     Connect the outputs to the red lights LEDR[3:0].
//
// (Reuse your MUXDFF from exams/2014_q4a.)
// https://hdlbits.01xz.net/wiki/Exams/2014_q4b

module MUXDFF (
    input clk,
    input w, R, E, L,
    output Q
);

    wire tmp;
    assign tmp = (E) ? w : Q;
    always @(posedge clk) begin
        Q <= (L) ? R : tmp;
    end
endmodule

module top_module (
    input [3:0] SW,
    input [3:0] KEY,
    output [3:0] LEDR
); //

    wire [3:0] Q;

    MUXDFF MUXDFF3 (
        .clk(KEY[0]),
        .w(KEY[3]),
        .R(SW[3]),
        .E(KEY[1]),
        .L(KEY[2]),
        .Q(Q[3])
    );

    MUXDFF MUXDFF2 (
        .clk(KEY[0]),
        .w(Q[3]),
        .R(SW[2]),
        .E(KEY[1]),
        .L(KEY[2]),
        .Q(Q[2])
    );

    MUXDFF MUXDFF1 (
        .clk(KEY[0]),
        .w(Q[2]),
        .R(SW[1]),
        .E(KEY[1]),
        .L(KEY[2]),
        .Q(Q[1])
    );

    MUXDFF MUXDFF0 (
        .clk(KEY[0]),
        .w(Q[1]),
        .R(SW[0]),
        .E(KEY[1]),
        .L(KEY[2]),
        .Q(Q[0])
    );

    assign LEDR = Q;
endmodule
