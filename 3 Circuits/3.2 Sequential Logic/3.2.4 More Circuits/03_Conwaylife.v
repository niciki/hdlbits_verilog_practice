// Conway's Game of Life is a two-dimensional cellular automaton.
//
// The "game" is played on a two-dimensional grid of cells, where each cell is either 1 (alive) or 0 (dead). At each time step, each cell changes state depending on how many neighbours it has:
//
//     0-1 neighbour: Cell becomes 0.
//     2 neighbours: Cell state does not change.
//     3 neighbours: Cell becomes 1.
//     4+ neighbours: Cell becomes 0.
//
// The game is formulated for an infinite grid. In this circuit, we will use a 16x16 grid. To make things more interesting, we will use a 16x16 toroid, where the sides wrap around to the other side of the grid. For example, the corner cell (0,0) has 8 neighbours: (15,1), (15,0), (15,15), (0,1), (0,15), (1,1), (1,0), and (1,15). The 16x16 grid is represented by a length 256 vector, where each row of 16 cells is represented by a sub-vector: q[15:0] is row 0, q[31:16] is row 1, etc. (This tool accepts SystemVerilog, so you may use 2D vectors if you wish.)
//
//     load: Loads data into q at the next clock edge, for loading initial state.
//     q: The 16x16 current state of the game, updated every clock cycle.
//
// The game state should advance by one timestep every clock cycle.
//
// John Conway, mathematician and creator of the Game of Life cellular automaton, passed away from COVID-19 on April 11, 2020.
//
//        c15|    c0    c1  c2  c3  c4  c5  c6  c7  c8  c9  c10 c11 c12 c13 c14 c15|c0
//row-1   255|    240	241	242	243	244	245	246	247	248	249	250	251	252	253	254	255|240
//-----------|---------------------------------------------------------------------|---
//row0    15 |    0	    1	2	3	4	5	6	7	8	9	10	11	12	13	14	15 |0
//row1    31 |    16	17	18	19	20	21	22	23	24	25	26	27	28	29	30	31 |16
//row2    47 |    32	33	34	35	36	37	38	39	40	41	42	43	44	45	46	47 |32
//row3    63 |    48	49	50	51	52	53	54	55	56	57	58	59	60	61	62	63 |48
//row4    79 |    64	65	66	67	68	69	70	71	72	73	74	75	76	77	78	79 |64
//row5    95 |    80	81	82	83	84	85	86	87	88	89	90	91	92	93	94	95 |80
//row6    111|    96	97	98	99	100	101	102	103	104	105	106	107	108	109	110	111|96
//row7    127|    112	113	114	115	116	117	118	119	120	121	122	123	124	125	126	127|112
//row8    143|    128	129	130	131	132	133	134	135	136	137	138	139	140	141	142	143|128
//row9    159|    144	145	146	147	148	149	150	151	152	153	154	155	156	157	158	159|144
//row10   175|    160	161	162	163	164	165	166	167	168	169	170	171	172	173	174	175|160
//row11   191|    176	177	178	179	180	181	182	183	184	185	186	187	188	189	190	191|176
//row12   207|    192	193	194	195	196	197	198	199	200	201	202	203	204	205	206	207|192
//row13   223|    208	209	210	211	212	213	214	215	216	217	218	219	220	221	222	223|208
//row14   239|    224	225	226	227	228	229	230	231	232	233	234	235	236	237	238	239|224
//row15   255|    240	241	242	243	244	245	246	247	248	249	250	251	252	253	254	255|240
//-----------|---------------------------------------------------------------------|
//row16   15 |    0	    1	2	3	4	5	6	7	8	9	10	11	12	13	14	15 |0
//
// https://hdlbits.01xz.net/wiki/Conwaylife


module sum_ones(
    input [7:0] vec,
    output reg [3:0] sum
    );

    integer i;

    always @(vec) begin
        sum = 0;
        for (i = 0; i < 8; i = i + 1) begin : gen_i
            sum = sum + vec[i]; //Add the bit to the count.
        end
    end
endmodule

module top_module(
    input clk,
    input load,
    input [255:0] data,
    output [255:0] q );

    wire [16:-1] data_exp [16:-1];
    assign data_exp[16] = data_exp[0];
    assign data_exp[-1] = data_exp[15];

    wire [7:0] neigh [15:0][15:0];  // 16x16 grid and 8 neighbours
    wire [3:0] sum [15:0][15:0];

    genvar row, col;
    generate
        for (row = 0; row < 16; row = row + 1) begin : gen_row
            assign data_exp[row] = {q[row*16], q[row*16+:16], q[(row+1)*16-1]};
            for (col = 0; col < 16; col = col + 1) begin : gen_col
                assign neigh[row][col] = {
                    data_exp[row-1][col-1], data_exp[row-1][col], data_exp[row-1][col+1],
                    data_exp[row][col-1], data_exp[row][col+1],
                    data_exp[row+1][col-1], data_exp[row+1][col], data_exp[row+1][col+1],
                };

                sum_ones sum_ones_i (
                    .vec(neigh[row][col]),
                    .sum(sum[row][col])
                );

                always @(posedge clk) begin
                    if (load) begin
                        q[row*16+col] <= data[row*16+col];
                    end else begin
                        case (sum[row][col])
                            4'd2: q[row*16+col] <= q[row*16+col];
                            4'd3: q[row*16+col] <= 1'b1;
                            default: q[row*16+col] <= 1'b0;
                        endcase
                    end
                end
            end
        end
    endgenerate
endmodule
