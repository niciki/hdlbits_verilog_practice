// Create a set of counters suitable for use as a 12-hour clock (with am/pm indicator). Your counters are clocked by a fast-running clk, with a pulse on ena whenever your clock should increment (i.e., once per second).
//
// reset resets the clock to 12:00 AM. pm is 0 for AM and 1 for PM. hh, mm, and ss are two BCD (Binary-Coded Decimal) digits each for hours (01-12), minutes (00-59), and seconds (00-59). Reset has higher priority than enable, and can occur even when not enabled.
//
//     The following timing diagram shows the rollover behaviour from 11:59:59 AM to 12:00:00 PM and the synchronous reset and enable behaviour.
// https://hdlbits.01xz.net/wiki/Count_clock

module top_module(
    input clk,
    input reset,
    input ena,
    output pm,
    output [7:0] hh,
    output [7:0] mm,
    output [7:0] ss);

    always @(posedge clk) begin
        if (reset) begin
            hh <= 8'h12;
            mm <= 8'h0;
            ss <= 8'h0;
            pm <= 1'b0;
        end else begin
            if (ena) begin
                ss <= ss + 1'b1;
                if (ss == 8'h59) begin
                    ss <= 8'h0;
                    mm <= mm + 1'b1;
                    if (mm == 8'h59) begin
                        mm <= 8'h0;
                        hh <= hh + 1'b1;
                        if (hh == 8'h11) begin
                            hh <= 8'h12;
                            pm <= ~pm;
                        end else if (hh == 8'h12) begin
                            hh <= 8'h1;
                        end else if (hh[3:0] == 4'h9) begin
                            hh[7:4] <= hh[7:4] + 1'b1;
                            hh[3:0] <= 4'h0;
                        end
                    end else if (mm[3:0] == 4'h9) begin
                        mm[7:4] <= mm[7:4] + 1'b1;
                        mm[3:0] <= 4'h0;
                    end
                end else if (ss[3:0] == 4'h9) begin
                    ss[7:4] <= ss[7:4] + 1'b1;
                    ss[3:0] <= 4'h0;
                end
            end
        end
    end
endmodule
