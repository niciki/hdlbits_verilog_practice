// Make a decade counter that counts 1 through 10, inclusive. The reset input is synchronous, and should reset the counter to 1.
// https://hdlbits.01xz.net/wiki/Count1to10

module top_module (
    input clk,
    input reset,
    output [3:0] q);

    always @(posedge clk) begin
        if (reset) begin
            q <= 4'd1;
        end else begin
            q <= q + 4'd1;
            if (q == 4'd10) begin
                q <= 4'd1;
            end
        end
    end
endmodule
