// Build a 4-digit BCD (binary-coded decimal) counter. Each decimal digit is encoded using 4 bits: q[3:0] is the ones digit, q[7:4] is the tens digit, etc. For digits [3:1], also output an enable signal indicating when each of the upper three digits should be incremented.
//
// You may want to instantiate or modify some one-digit decade counters.
// https://hdlbits.01xz.net/wiki/Countbcd

module top_module (
    input clk,
    input reset,   // Synchronous active-high reset
    output [3:1] ena,
    output [15:0] q);

    reg [3:0] q1000 = 0;
    reg [3:0] q100 = 0;
    reg [3:0] q10 = 0;
    reg [3:0] q1 = 0;

    assign q[15:12] = q1000;
    assign q[11:8] = q100;
    assign q[7:4] = q10;
    assign q[3:0] = q1;

    always @(posedge clk) begin
        if (reset) begin
            q1000 <= 0;
            q100 <= 0;
            q10 <= 0;
            q1 <= 0;
        end else begin
            q1 <= q1 + 1'd1;
            if (q1 == 4'd9) begin
                q1 <= 0;
                q10 <= q10 + 1'd1;
                if (q10 == 4'd9) begin
                    q10 <= 0;
                    q100 <= q100 + 1'd1;
                    if (q100 == 4'd9) begin
                        q100 <= 0;
                        q1000 <= q1000 + 1'd1;
                        if (q1000 == 4'd9) begin
                            q1000 <= 0;
                            q100 <= 0;
                            q10 <= 0;
                            q1 <= 0;
                        end
                    end
                end
            end
        end
    end

    assign ena[1] = (q1 == 4'd9);
    assign ena[2] = (q1 == 4'd9) && (q10 == 4'd9);
    assign ena[3] = (q1 == 4'd9) && (q10 == 4'd9) && (q100 == 4'd9);
endmodule
