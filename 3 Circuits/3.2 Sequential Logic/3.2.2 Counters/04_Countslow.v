// Build a decade counter that counts from 0 through 9, inclusive, with a period of 10. The reset input is synchronous, and should reset the counter to 0. We want to be able to pause the counter rather than always incrementing every clock cycle, so the slowena input indicates when the counter should increment.
// https://hdlbits.01xz.net/wiki/Countslow

module top_module (
    input clk,
    input slowena,
    input reset,
    output [3:0] q);

    always @(posedge clk) begin
        if (reset) begin
            q <= 0;
        end else begin
            if (slowena) begin
                q <= q + 4'd1;
            end
            if (slowena && (q == 4'd9)) begin
                q <= 0;
            end
        end
    end
endmodule
