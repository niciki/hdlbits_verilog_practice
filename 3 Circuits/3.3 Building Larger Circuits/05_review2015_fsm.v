// This is the fourth component in a series of five exercises that builds a complex counter out of several smaller circuits. See the final exercise for the overall design.
//
// You may wish to do FSM: Enable shift register and FSM: Sequence recognizer first.
//
// We want to create a timer that:
//
//     is started when a particular pattern (1101) is detected,
//     shifts in 4 more bits to determine the duration to delay,
//     waits for the counters to finish counting, and
//     notifies the user and waits for the user to acknowledge the timer.
//
// In this problem, implement just the finite-state machine that controls the timer. The data path (counters and some comparators) are not included here.
//
// The serial data is available on the data input pin. When the pattern 1101 is received, the state machine must then assert output shift_ena for exactly 4 clock cycles.
//
// After that, the state machine asserts its counting output to indicate it is waiting for the counters, and waits until input done_counting is high.
//
// At that point, the state machine must assert done to notify the user the timer has timed out, and waits until input ack is 1 before being reset to look for the next occurrence of the start sequence (1101).
//
// The state machine should reset into a state where it begins searching for the input sequence 1101.
//
// Here is an example of the expected inputs and outputs. The 'x' states may be slightly confusing to read. They indicate that the FSM should not care about that particular input signal in that cycle. For example, once a 1101 pattern is detected, the FSM no longer looks at the data input until it resumes searching after everything else is done.
// https://hdlbits.01xz.net/wiki/Exams/review2015_fsm

module top_module (
    input clk,
    input reset,      // Synchronous reset
    input data,
    output shift_ena,
    output counting,
    input done_counting,
    output done,
    input ack );

    parameter
        ST_INIT = 4'd0,
        ST_1 = 4'd1,
        ST_11 = 4'd2,
        ST_110 = 4'd3,
        ST_SE1 = 4'd4,
        ST_SE2 = 4'd5,
        ST_SE3 = 4'd6,
        ST_SE4 = 4'd7,
        ST_COUNT = 4'd8,
        ST_DONE = 4'd9;

    reg [3:0] state;

    always @(posedge clk) begin
        if (reset) begin
            state <= ST_INIT;
            shift_ena <= 1'b0;
            counting <= 1'b0;
            done <= 1'b0;
        end else begin
            shift_ena <= 1'b0;
            counting <= 1'b0;
            done <= 1'b0;
            case (state)
                ST_INIT: begin
                    if (data) begin
                        state <= ST_1;
                    end
                end
                ST_1: begin
                    if (data) begin
                        state <= ST_11;
                    end else begin
                        state <= ST_INIT;
                    end
                end
                ST_11: begin
                    if (data) begin
                        state <= ST_11;
                    end else begin
                        state <= ST_110;
                    end
                end
                ST_110: begin
                    if (data) begin
                        shift_ena <= 1'b1;
                        state <= ST_SE1;
                    end else begin
                        state <= ST_INIT;
                    end
                end
                ST_SE1: begin
                    shift_ena <= 1'b1;
                    state <= ST_SE2;
                end
                ST_SE2: begin
                    shift_ena <= 1'b1;
                    state <= ST_SE3;
                end
                ST_SE3: begin
                    shift_ena <= 1'b1;
                    state <= ST_SE4;
                end
                ST_SE4: begin
                    counting <= 1'b1;
                    state <= ST_COUNT;
                end
                ST_COUNT: begin
                    counting <= 1'b1;
                    if (done_counting) begin
                        counting <= 1'b0;
                        done <= 1'b1;
                        state <= ST_DONE;
                    end
                end
                ST_DONE: begin
                    done <= 1'b1;
                    if (ack) begin
                        done <= 1'b0;
                        state <= ST_INIT;
                    end
                end
                default: begin
                    state <= ST_INIT;
                    shift_ena <= 1'b0;
                    counting <= 1'b0;
                    done <= 1'b0;
                end
            endcase
        end
    end
endmodule
