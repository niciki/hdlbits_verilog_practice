// This is the fifth component in a series of five exercises that builds a complex counter out of several smaller circuits. You may wish to do the four previous exercises first (counter, sequence recognizer FSM, FSM delay, and combined FSM).
//
// We want to create a timer with one input that:
//
// is started when a particular input pattern (1101) is detected,
// shifts in 4 more bits to determine the duration to delay,
// waits for the counters to finish counting, and
// notifies the user and waits for the user to acknowledge the timer.
//
// The serial data is available on the data input pin. When the pattern 1101 is received, the circuit must then shift in the next 4 bits, most-significant-bit first. These 4 bits determine the duration of the timer delay. I'll refer to this as the delay[3:0].
//
// After that, the state machine asserts its counting output to indicate it is counting. The state machine must count for exactly (delay[3:0] + 1) * 1000 clock cycles. e.g., delay=0 means count 1000 cycles, and delay=5 means count 6000 cycles. Also output the current remaining time. This should be equal to delay for 1000 cycles, then delay-1 for 1000 cycles, and so on until it is 0 for 1000 cycles. When the circuit isn't counting, the count[3:0] output is don't-care (whatever value is convenient for you to implement).
//
// At that point, the circuit must assert done to notify the user the timer has timed out, and waits until input ack is 1 before being reset to look for the next occurrence of the start sequence (1101).
//
// The circuit should reset into a state where it begins searching for the input sequence 1101.
//
// Here is an example of the expected inputs and outputs. The 'x' states may be slightly confusing to read. They indicate that the FSM should not care about that particular input signal in that cycle. For example, once the 1101 and delay[3:0] have been read, the circuit no longer looks at the data input until it resumes searching after everything else is done. In this example, the circuit counts for 2000 clock cycles because the delay[3:0] value was 4'b0001. The last few cycles starts another count with delay[3:0] = 4'b1110, which will count for 15000 cycles.
// https://hdlbits.01xz.net/wiki/Exams/review2015_fancytimer

module count1k (
    input clk,
    input reset,
    input counting,
    output done,
    output [9:0] q);

    always @(posedge clk) begin
        done <= 0;
        if (reset) begin
            q <= 0;
        end else if (counting) begin
            q <= q + 1'd1;
            if (q == 10'd999) begin
                q <= 0;
            end
            if (q == 10'd998) begin
                done <= 1'b1;
            end
        end
    end
endmodule

module shiftcount (
    input clk,
    input shift_ena,
    input count_ena,
    input data,
    output [3:0] q);

    always @(posedge clk) begin
        if (shift_ena) begin
            q[0] <= data;
            q[3:1] <= q[2:0];
        end else if (count_ena) begin
            q <= q - 1'd1;
            if (q == 0) begin
                q <= 4'hF;
            end
        end
    end
endmodule

module fsm (
    input clk,
    input reset,
    input data,
    output shift_ena,
    output counting,
    input done_counting,
    output done,
    input ack );

    parameter
        ST_INIT = 4'd0,
        ST_1 = 4'd1,
        ST_11 = 4'd2,
        ST_110 = 4'd3,
        ST_SE1 = 4'd4,
        ST_SE2 = 4'd5,
        ST_SE3 = 4'd6,
        ST_SE4 = 4'd7,
        ST_COUNT = 4'd8,
        ST_DONE = 4'd9;

    reg [3:0] state;

    always @(posedge clk) begin
        if (reset) begin
            state <= ST_INIT;
            shift_ena <= 1'b0;
            counting <= 1'b0;
            done <= 1'b0;
        end else begin
            shift_ena <= 1'b0;
            counting <= 1'b0;
            done <= 1'b0;
            case (state)
                ST_INIT: begin
                    if (data) begin
                        state <= ST_1;
                    end
                end
                ST_1: begin
                    if (data) begin
                        state <= ST_11;
                    end else begin
                        state <= ST_INIT;
                    end
                end
                ST_11: begin
                    if (data) begin
                        state <= ST_11;
                    end else begin
                        state <= ST_110;
                    end
                end
                ST_110: begin
                    if (data) begin
                        shift_ena <= 1'b1;
                        state <= ST_SE1;
                    end else begin
                        state <= ST_INIT;
                    end
                end
                ST_SE1: begin
                    shift_ena <= 1'b1;
                    state <= ST_SE2;
                end
                ST_SE2: begin
                    shift_ena <= 1'b1;
                    state <= ST_SE3;
                end
                ST_SE3: begin
                    shift_ena <= 1'b1;
                    state <= ST_SE4;
                end
                ST_SE4: begin
                    counting <= 1'b1;
                    state <= ST_COUNT;
                end
                ST_COUNT: begin
                    counting <= 1'b1;
                    if (done_counting) begin
                        counting <= 1'b0;
                        done <= 1'b1;
                        state <= ST_DONE;
                    end
                end
                ST_DONE: begin
                    done <= 1'b1;
                    if (ack) begin
                        done <= 1'b0;
                        state <= ST_INIT;
                    end
                end
                default: begin
                    state <= ST_INIT;
                    shift_ena <= 1'b0;
                    counting <= 1'b0;
                    done <= 1'b0;
                end
            endcase
        end
    end
endmodule

module top_module (
    input clk,
    input reset,      // Synchronous reset
    input data,
    output [3:0] count,
    output counting,
    output done,
    input ack );

    wire count1k_inst_done;
    wire [9:0] count1k_inst_q;
    count1k count1k_inst (
      .clk (clk),
      .reset (reset),
      .counting (fsm_inst_counting),
      .done (count1k_inst_done),
      .q (count1k_inst_q)
    );

    wire [3:0] shiftcount_inst_q;
    shiftcount shiftcount_inst (
      .clk (clk),
      .shift_ena (fsm_inst_shift_ena),
      .count_ena (count1k_inst_done),
      .data (data),
      .q (shiftcount_inst_q)
    );

    wire fsm_inst_shift_ena;
    wire fsm_inst_counting;
    wire fsm_inst_done;
    fsm fsm_inst (
      .clk (clk),
      .reset (reset),
      .data (data),
      .shift_ena (fsm_inst_shift_ena),
      .counting (fsm_inst_counting),
      .done_counting ((shiftcount_inst_q == 0) && count1k_inst_done),
      .done (fsm_inst_done),
      .ack (ack)
    );
    assign count = shiftcount_inst_q;
    assign counting = fsm_inst_counting;
    assign done = fsm_inst_done;
endmodule
