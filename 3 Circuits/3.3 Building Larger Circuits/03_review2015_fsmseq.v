// This is the second component in a series of five exercises that builds a complex counter out of several smaller circuits. See the final exercise for the overall design.
// Build a finite-state machine that searches for the sequence 1101 in an input bit stream. When the sequence is found, it should set start_shifting to 1, forever, until reset. Getting stuck in the final state is intended to model going to other states in a bigger FSM that is not yet implemented. We will be extending this FSM in the next few exercises.
// https://hdlbits.01xz.net/wiki/Exams/review2015_fsmseq

module top_module (
    input clk,
    input reset,      // Synchronous reset
    input data,
    output start_shifting);

    parameter
        ST_IDLE = 3'd0,
        ST_1 = 3'd1,
        ST_11 = 3'd2,
        ST_110 = 3'd3,
        ST_START = 3'd4;

    reg [2:0] state;

    always @(posedge clk) begin
        if (reset) begin
            state <= ST_IDLE;
            start_shifting <= 1'b0;
        end else begin
            start_shifting <= 1'b0;
            case (state)
                ST_IDLE: begin
                    if (data) begin
                        state <= ST_1;
                    end else begin
                        state <= ST_IDLE;
                    end
                end
                ST_1: begin
                    if (data) begin
                        state <= ST_11;
                    end else begin
                        state <= ST_IDLE;
                    end
                end
                ST_11: begin
                    if (data) begin
                        state <= ST_11;
                    end else begin
                        state <= ST_110;
                    end
                end
                ST_110: begin
                    if (data) begin
                        state <= ST_START;
                        start_shifting <= 1'b1;
                    end else begin
                        state <= ST_1;
                    end
                end
                ST_START: begin
                    start_shifting <= 1'b1;
                end
                default: begin
                    state <= ST_IDLE;
                    start_shifting <= 1'b0;
                end
            endcase
        end
    end
endmodule
