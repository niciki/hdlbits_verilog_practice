// Build a counter that counts from 0 to 999, inclusive, with a period of 1000 cycles. The reset input is synchronous, and should reset the counter to 0.
// https://hdlbits.01xz.net/wiki/Exams/review2015_count1k

module top_module (
    input clk,
    input reset,
    output [9:0] q);

    always @(posedge clk) begin
        if (reset) begin
            q <= 0;
        end else begin
            q <= q + 1'd1;
            if (q == 10'd999) begin
                q <= 0;
            end
        end
    end
endmodule
