// Implement the circuit described by the Karnaugh map below.
// https://hdlbits.01xz.net/wiki/File:Kmap1.png

module top_module(
    input a,
    input b,
    input c,
    output out  );

    assign out = ~(~a & ~b & ~c);
endmodule
