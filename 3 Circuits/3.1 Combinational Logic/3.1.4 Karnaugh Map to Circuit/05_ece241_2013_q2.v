// A single-output digital system with four inputs (a,b,c,d) generates a logic-1 when 2, 7, or 15 appears on the inputs, and a logic-0 when 0, 1, 4, 5, 6, 9, 10, 13, or 14 appears. The input conditions for the numbers 3, 8, 11, and 12 never occur in this system. For example, 7 corresponds to a,b,c,d being set to 0,1,1,1, respectively.
//
// Determine the output out_sop in minimum SOP form, and the output out_pos in minimum POS form.
// https://hdlbits.01xz.net/wiki/Exams/ece241_2013_q2

module top_module (
    input a,
    input b,
    input c,
    input d,
    output out_sop,
    output out_pos
);

    wire [3:0] num = {a, b, c, d};

    assign out_sop = (num == 4'd2) || (num == 4'd3) || (num == 4'd7) || (num == 4'd11) || (num == 4'd15);
    assign out_pos = (num == 4'd2) || (num == 4'd3) || (num == 4'd7) || (num == 4'd11) || (num == 4'd15);
endmodule
