// Implement the circuit described by the Karnaugh map below.
// https://hdlbits.01xz.net/wiki/File:Kmap2.png

module top_module(
    input a,
    input b,
    input c,
    input d,
    output out  );

    assign out = ~((a&c&~d) | (b&~c&d) | (a&b&~c&~d) | (~a&~b&c&d));
endmodule
