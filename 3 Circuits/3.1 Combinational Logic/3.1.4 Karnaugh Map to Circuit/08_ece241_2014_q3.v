// For the following Karnaugh map, give the circuit implementation using one 4-to-1 multiplexer and as many 2-to-1 multiplexers as required, but using as few as possible. You are not allowed to use any other logic gate and you must use a and b as the multiplexer selector inputs, as shown on the 4-to-1 multiplexer below.
// You are implementing just the portion labelled top_module, such that the entire circuit (including the 4-to-1 mux) implements the K-map.
// https://hdlbits.01xz.net/wiki/Exams/ece241_2014_q3

module top_module (
    input c,
    input d,
    output [3:0] mux_in
);

    wire [1:0] cd = {c, d};

    always @(*) begin
        case (cd)
            2'b00: begin
                mux_in <= 4'b0100;
            end
            2'b01: begin
                mux_in <= 4'b0001;
            end
            2'b11: begin
                mux_in <= 4'b1001;
            end
            2'b10: begin
                mux_in <= 4'b0101;
            end
            default: begin
                mux_in <= 4'b0100;
            end
        endcase
    end
endmodule
