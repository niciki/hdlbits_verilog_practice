// Consider the function f shown in the Karnaugh map below.
//
// Exams m2014q3.png
//
// Implement this function. d is don't-care, which means you may choose to output whatever value is convenient.
// https://hdlbits.01xz.net/wiki/Exams/m2014_q3

module top_module (
    input [4:1] x,
    output f );

    assign f = ~x[1]&x[3] | x[2]&x[4];
endmodule
