// Implement the circuit described by the Karnaugh map below.
// https://hdlbits.01xz.net/wiki/File:Kmap3.png

module top_module(
    input a,
    input b,
    input c,
    input d,
    output out  );

    assign out = ~((~a&b) | (~a&~c));
endmodule
