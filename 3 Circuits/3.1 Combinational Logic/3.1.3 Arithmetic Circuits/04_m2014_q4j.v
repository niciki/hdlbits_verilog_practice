// Implement the following circuit:
// https://hdlbits.01xz.net/wiki/File:Exams_m2014q4j.png
// ("FA" is a full adder)

module FA(
    input a, b, cin,
    output cout, sum );

    assign cout = (a & b) | (cin & a) | (cin & b);
    assign sum = a ^ b ^ cin;
endmodule

module top_module (
    input [3:0] x,
    input [3:0] y,
    output [4:0] sum);

    wire [3:0] cin_int;
    wire [3:0] cout_int;

    assign cin_int = {cout_int[2:0], 1'b0};

    genvar i;
    generate
        for (i = 0; i < 4; i = i + 1) begin : gen_fadd
            FA FA_i ( .a(x[i]), .b(y[i]), .cin(cin_int[i]), .sum(sum[i]), .cout(cout_int[i]) );
        end
    endgenerate

    assign sum[4] = cout_int[3];
endmodule
