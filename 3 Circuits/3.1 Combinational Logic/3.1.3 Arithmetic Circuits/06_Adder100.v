// Create a 100-bit binary adder. The adder adds two 100-bit numbers and a carry-in to produce a 100-bit sum and carry out.

module fadd(
    input a, b, cin,
    output cout, sum );

    assign cout = (a & b) | (cin & a) | (cin & b);
    assign sum = a ^ b ^ cin;
endmodule

module top_module(
    input [99:0] a, b,
    input cin,
    output cout,
    output [99:0] sum );

    wire [99:0] cin_int;
    wire [99:0] cout_int;

    assign cin_int = {cout_int[98:0], cin};

    genvar i;
    generate
        for(i = 0; i < 100; i = i + 1) begin : gen_fadd
            fadd fadd_i( .a(a[i]), .b(b[i]), .cin(cin_int[i]), .cout(cout_int[i]), .sum(sum[i]) );
        end
    endgenerate

    assign cout = cout_int[99];
endmodule
