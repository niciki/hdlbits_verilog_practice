// Now that you know how to build a full adder, make 3 instances of it to create a 3-bit binary ripple-carry adder. The adder adds two 3-bit numbers and a carry-in to produce a 3-bit sum and carry out. To encourage you to actually instantiate full adders, also output the carry-out from each full adder in the ripple-carry adder. cout[2] is the final carry-out from the last full adder, and is the carry-out you usually see.

module Fadd(
    input a, b, cin,
    output cout, sum );

    assign cout = (a & b) | (cin & a) | (cin & b);
    assign sum = a ^ b ^ cin;
endmodule

module top_module(
    input [2:0] a, b,
    input cin,
    output [2:0] cout,
    output [2:0] sum );

    wire [2:0] cin_int;
    wire [2:0] cout_int;

    assign cin_int = {cout_int[1:0], cin};

    genvar i;
    generate
        for (i = 0; i < 3; i = i + 1) begin : gen_fadd
            Fadd Fadd_i ( .a(a[i]), .b(b[i]), .cin(cin_int[i]), .sum(sum[i]), .cout(cout_int[i]) );
        end
    endgenerate

    assign cout = cout_int;
endmodule
