// You are provided with a BCD (binary-coded decimal) one-digit adder named bcd_fadd that adds two BCD digits and carry-in, and produces a sum and carry-out.
//
// module bcd_fadd (
//     input [3:0] a,
//     input [3:0] b,
//     input     cin,
//     output   cout,
//     output [3:0] sum );
//
// Instantiate 4 copies of bcd_fadd to create a 4-digit BCD ripple-carry adder. Your adder should add two 4-digit BCD numbers (packed into 16-bit vectors) and a carry-in to produce a 4-digit sum and carry out.

module top_module (
    input [15:0] a, b,
    input cin,
    output cout,
    output [15:0] sum );

    wire [3:0] cin_int;
    wire [3:0] cout_int;

    assign cin_int = {cout_int[2:0], cin};

    genvar i;
    generate
        for (i = 0; i < 4; i = i + 1) begin : gen_bcd_fadd
            bcd_fadd bcd_fadd_i (
                .a(a[4 * i +: 4]),
                .b(b[4 * i +: 4]),
                .cin(cin_int[i]),
                .cout(cout_int[i]),
                .sum(sum[4 * i +: 4])
            );
        end
    endgenerate

    assign cout = cout_int[3];
endmodule
