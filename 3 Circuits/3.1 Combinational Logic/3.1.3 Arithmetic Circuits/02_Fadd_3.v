// Create a full adder. A full adder adds three bits (including carry-in) and produces a sum and carry-out.

module top_module(
    input a, b, cin,
    output cout, sum );

    assign cout = (cin & (a ^ b)) | (a & b);
    assign sum = a ^ b ^ cin;
endmodule
