// Implement the following circuit:
// https://hdlbits.01xz.net/wiki/File:Exams_m2014q4g.png

module top_module (
    input in1,
    input in2,
    input in3,
    output out);

    assign out = ~(in1 ^ in2) ^ in3;
endmodule
