// Implement the following circuit:
// https://hdlbits.01xz.net/wiki/File:Exams_m2014q4i.png

module top_module (
    output out);

    assign out = 1'b0;
endmodule
