// Circuit B can be described by the following simulation waveform: Mt2015 q4b.png
// https://hdlbits.01xz.net/wiki/File:Mt2015_q4b.png
// Implement this circuit.

module top_module ( input x, input y, output z );

    assign z = ~(x ^ y);
endmodule
