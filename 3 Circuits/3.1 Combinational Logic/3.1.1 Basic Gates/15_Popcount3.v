// A "population count" circuit counts the number of '1's in an input vector. Build a population count circuit for a 3-bit input vector.

module top_module(
    input [2:0] in,
    output [1:0] out );

    always @(*) begin
        out = 0;
        for ( int i = 0; i < 3; i = i + 1) begin
            if (in[i] == 1) begin
                out = out + 1'd1;
            end
            else begin
                out = out + 1'd0;
            end
        end
    end
endmodule
