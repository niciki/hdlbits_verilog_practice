// The top-level design consists of two instantiations each of subcircuits A and B, as shown below.
// https://hdlbits.01xz.net/wiki/File:Mt2015_q4.png
// Implement this circuit.

module A (input x, input y, output z);

    assign z = (x ^ y) & x;
endmodule

module B ( input x, input y, output z );

    assign z = ~(x ^ y);
endmodule

module top_module (input x, input y, output z);

    wire z_a1;
    wire z_b1;
    wire z_a2;
    wire z_b2;

    A IA1 (.x(x), .y(y), .z(z_a1));
    B IB1 (.x(x), .y(y), .z(z_b1));
    A IA2 (.x(x), .y(y), .z(z_a2));
    B IB2 (.x(x), .y(y), .z(z_b2));

    assign z = (z_a1 | z_b1) ^ (z_a2 & z_b2);
endmodule
