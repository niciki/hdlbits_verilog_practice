// Implement the following circuit:
// https://hdlbits.01xz.net/wiki/File:Exams_m2014q4f.png

module top_module (
    input in1,
    input in2,
    output out);

    assign out = in1 & ~in2;
endmodule
