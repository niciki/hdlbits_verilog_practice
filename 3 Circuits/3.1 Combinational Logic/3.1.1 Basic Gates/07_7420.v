// Create a module with the same functionality as the 7420 chip. It has 8 inputs and 2 outputs.
// https://hdlbits.01xz.net/wiki/File:7420.png

module top_module (
    input p1a, p1b, p1c, p1d,
    output p1y,
    input p2a, p2b, p2c, p2d,
    output p2y );

    assign p1y = ~&{p1a, p1b, p1c, p1d};
    assign p2y = ~&{p2a, p2b, p2c, p2d};
endmodule
