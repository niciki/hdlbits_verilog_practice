// Implement the following circuit:
// https://hdlbits.01xz.net/wiki/File:Exams_m2014q4h.png

module top_module (
    input in,
    output out);

    assign out = in;
endmodule
