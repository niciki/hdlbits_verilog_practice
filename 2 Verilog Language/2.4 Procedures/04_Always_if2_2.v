// Fix the bugs so that you will shut off the computer only if it's really overheated, and stop driving if you've arrived at your destination or you need to refuel.
// https://hdlbits.01xz.net/wiki/File:Always_if2.png

// synthesis verilog_input_version verilog_2001
module top_module (
    input      cpu_overheated,
    output reg shut_off_computer,
    input      arrived,
    input      gas_tank_empty,
    output reg keep_driving  ); //

    always @(*) begin
        shut_off_computer = (cpu_overheated) ? 1'b1 : 1'b0;
    end

    always @(*) begin
        keep_driving = (~arrived & ~gas_tank_empty) ? 1'b1 : 1'b0;
    end
endmodule
