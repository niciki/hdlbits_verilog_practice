// The module mod_a is provided for you — you must instantiate it.
//
//     When connecting modules, only the ports on the module are important. You do not need to know the code inside the module. The code for module mod_a looks like this:
//     Module moda.png
//
//     module mod_a ( input in1, input in2, output out );
//         // Module body
//     endmodule
// https://hdlbits.01xz.net/wiki/File:Module.png

module top_module ( input a, input b, output out );

    mod_a instance2 (
        .out(out),
        .in1(a),
        .in2(b)
    );
endmodule
