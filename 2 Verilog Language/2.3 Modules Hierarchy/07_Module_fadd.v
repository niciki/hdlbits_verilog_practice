// Connect the add16 modules together as shown in the diagram below. The provided module add16 has the following declaration:
//
// module add16 ( input[15:0] a, input[15:0] b, input cin, output[15:0] sum, output cout );
//
// Within each add16, 16 full adders (module add1, not provided) are instantiated to actually perform the addition. You must write the full adder module that has the following declaration:
//
// module add1 ( input a, input b, input cin, output sum, output cout );
//
// Recall that a full adder computes the sum and carry-out of a+b+cin.
//
// In summary, there are three modules in this design:
//
//     top_module — Your top-level module that contains two of...
//     add16, provided — A 16-bit adder module that is composed of 16 of...
//     add1 — A 1-bit full adder module.
//
//
// If your submission is missing a module add1, you will get an error message that says Error (12006): Node instance "user_fadd[0].a1" instantiates undefined entity "add1".
// https://hdlbits.01xz.net/wiki/File:Module_fadd.png

module top_module (
    input [31:0] a,
    input [31:0] b,
    output [31:0] sum
);//

    wire cout_1;
    add16 add16_1 ( .a(a[15:0]), .b(b[15:0]), .cin(1'b0), .sum(sum[15:0]), .cout(cout_1) );
    add16 add16_2 ( .a(a[31:16]), .b(b[31:16]), .cin(cout_1), .sum(sum[31:16]), .cout() );
endmodule

module add1 ( input a, input b, input cin,   output sum, output cout );

    assign sum = (~cin & a & ~b) | (~cin & ~a & b) | (cin & a & b) | (cin & ~a & ~b);
    assign cout = (~cin & a & b) | (cin & a & ~b) | (cin & ~a & b) | (cin & a & b);
endmodule
