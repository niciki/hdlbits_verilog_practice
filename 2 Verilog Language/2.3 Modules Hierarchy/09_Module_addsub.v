// Build the adder-subtractor below.
//
// You are provided with a 16-bit adder module, which you need to instantiate twice:
//
// module add16 ( input[15:0] a, input[15:0] b, input cin, output[15:0] sum, output cout );
//
// Use a 32-bit wide XOR gate to invert the b input whenever sub is 1. (This can also be viewed as b[31:0] XORed with sub replicated 32 times.
//
// Also connect the sub input to the carry-in of the adder.
// https://hdlbits.01xz.net/wiki/File:Module_addsub.png

module top_module(
    input [31:0] a,
    input [31:0] b,
    input sub,
    output [31:0] sum
);

    wire [31:0] b_int;
    assign b_int = b ^ {32{sub}};
    wire cout_1;
    add16 add16_1 ( .a(a[15:0]), .b(b_int[15:0]), .cin(sub), .sum(sum[15:0]), .cout(cout_1) );
    add16 add16_2 ( .a(a[31:16]), .b(b_int[31:16]), .cin(cout_1), .sum(sum[31:16]), .cout() );
endmodule
