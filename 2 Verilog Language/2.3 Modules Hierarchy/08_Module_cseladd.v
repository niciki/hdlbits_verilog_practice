// In this exercise, you are provided with the same module add16 as the previous exercise, which adds two 16-bit numbers with carry-in and produces a carry-out and 16-bit sum. You must instantiate three of these to build the carry-select adder, using your own 16-bit 2-to-1 multiplexer.
//
// Connect the modules together as shown in the diagram below. The provided module add16 has the following declaration:
//
// module add16 ( input[15:0] a, input[15:0] b, input cin, output[15:0] sum, output cout );
// https://hdlbits.01xz.net/wiki/File:Module_cseladd.png

module top_module(
    input [31:0] a,
    input [31:0] b,
    output [31:0] sum
);

    wire cout_1;
    add16 add16_0 ( .a(a[15:0]), .b(b[15:0]), .cin(1'b0), .sum(sum[15:0]), .cout(cout_1) );

    wire [15:0] sum_1;
    wire [15:0] sum_2;
    add16 add16_1 ( .a(a[31:16]), .b(b[31:16]), .cin(1'b0), .sum(sum_1), .cout() );
    add16 add16_2 ( .a(a[31:16]), .b(b[31:16]), .cin(1'b1), .sum(sum_2), .cout() );

    assign sum[31:16] = (cout_1) ? sum_2 : sum_1;
endmodule
