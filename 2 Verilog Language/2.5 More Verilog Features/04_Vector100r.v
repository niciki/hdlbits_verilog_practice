// Given a 100-bit input vector [99:0], reverse its bit ordering.

module top_module(
    input [99:0] in,
    output [99:0] out
);

    always@(in) begin
        for (int j = 0; j <= 99; j = j + 1) begin
            out[j] = in[99 - j];
        end
    end
endmodule
