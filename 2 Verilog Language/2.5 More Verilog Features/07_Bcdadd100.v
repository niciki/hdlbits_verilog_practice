// You are provided with a BCD one-digit adder named bcd_fadd that adds two BCD digits and carry-in, and produces a sum and carry-out.
//
// module bcd_fadd (
//     input [3:0] a,
//     input [3:0] b,
//     input     cin,
//     output   cout,
//     output [3:0] sum );
//
// Instantiate 100 copies of bcd_fadd to create a 100-digit BCD ripple-carry adder. Your adder should add two 100-digit BCD numbers (packed into 400-bit vectors) and a carry-in to produce a 100-digit sum and carry out.

module top_module(
    input [399:0] a, b,
    input cin,
    output cout,
    output [399:0] sum );

    wire [99:0] cin_int;
    wire [99:0] cout_int;
    assign cin_int[0] = cin;
    assign cin_int[99:1] = cout_int[98:0];
    assign cout = cout_int[99];

    genvar i;
    generate
        for (i = 0; i < 100; i = i + 1) begin : gen_bcd_fadd
            bcd_fadd bcd_fadd_i(
                .a(a[4*i +: 4]),
                .b(b[4*i +: 4]),
                .cin(cin_int[i]),
                .cout(cout_int[i]),
                .sum(sum[4*i +: 4])
            );
        end
    endgenerate
endmodule
