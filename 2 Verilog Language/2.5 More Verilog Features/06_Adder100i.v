// Create a 100-bit binary ripple-carry adder by instantiating 100 full adders. The adder adds two 100-bit numbers and a carry-in to produce a 100-bit sum and carry out. To encourage you to actually instantiate full adders, also output the carry-out from each full adder in the ripple-carry adder. cout[99] is the final carry-out from the last full adder, and is the carry-out you usually see.

module fadd(
    input a, b, cin,
    output cout, sum );

    assign cout = (a & b) | (cin & a) | (cin & b);
    assign sum = cin ^ a ^ b;
endmodule

module top_module(
    input [99:0] a, b,
    input cin,
    output [99:0] cout,
    output [99:0] sum );

    fadd fadd_1(a[0], b[0], cin, cout[0], sum[0]);

    genvar i;
    generate
        for(i = 1; i < 100; i = i + 1) begin : generate_block_identifier
            fadd fadd_i( a[i], b[i], cout[i-1], cout[i], sum[i] );
        end
    endgenerate
endmodule
