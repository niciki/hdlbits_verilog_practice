// The waveform below sets clk, in, and s:
// Module q7 has the following declaration:
//
// module q7 (
//     input clk,
//     input in,
//     input [2:0] s,
//     output out
// );
//
// Write a testbench that instantiates module q7 and generates these input signals exactly as shown in the waveform above.
// https://hdlbits.01xz.net/wiki/Tb/tb2

module top_module();

reg clk;
reg in;
reg [2:0] s;

initial begin
    clk = 0;
    forever #5 clk = ~clk;
end

initial begin
    in = 0;
    s = 3'd2;
    #10
    s = 3'd6;
    #10
    in = 1;
    s = 3'd2;
    #10
    in = 0;
    s = 3'd7;
    #10
    in = 1;
    s = 3'd0;
    #30
    in = 0;
end

q7 q7_inst (
    .clk(clk),
    .in(in),
    .s(s),
    .out()
);
endmodule
