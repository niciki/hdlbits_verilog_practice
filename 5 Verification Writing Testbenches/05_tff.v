// You are given a T flip-flop module with the following declaration:
//
// module tff (
//     input clk,
//     input reset,   // active-high synchronous reset
//     input t,       // toggle
//     output q
// );
//
// Write a testbench that instantiates one tff and will reset the T flip-flop then toggle it to the "1" state.
// https://hdlbits.01xz.net/wiki/Tb/tff

module top_module ();

reg clk;
reg reset;
reg t;

initial begin
    clk = 0;
    forever begin
        #5 clk = ~clk;
    end
end

initial begin
    reset = 0;
    #3 reset = 1;
    #5 reset = 0;
end

initial begin
    t = 0;
    #18 t = 1;
    #20 t = 0;
end

tff tff_inst (
    .clk(clk),
    .reset(reset),   // active-high synchronous reset
    .t(t),       // toggle
    .q()
);
endmodule
