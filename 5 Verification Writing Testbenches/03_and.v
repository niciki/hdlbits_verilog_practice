// You are given the following AND gate you wish to test:
//
// module andgate (
//     input [1:0] in,
//     output out
// );
//
// Write a testbench that instantiates this AND gate and tests all 4 input combinations, by generating the following timing diagram:
// https://hdlbits.01xz.net/wiki/Tb/and

module top_module();
reg [1:0] in;
wire out;
initial begin
    in = 2'd0;
    #10
    in = 2'd1;
    #10
    in = 2'd2;
    #10
    in = 2'd3;
end
andgate andgate_inst (in, out);
endmodule