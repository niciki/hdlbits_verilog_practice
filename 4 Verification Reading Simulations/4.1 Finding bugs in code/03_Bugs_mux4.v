// This 4-to-1 multiplexer doesn't work. Fix the bug(s).
//
// You are provided with a bug-free 2-to-1 multiplexer:
//
// module mux2 (
//     input sel,
//     input [7:0] a,
//     input [7:0] b,
//     output [7:0] out
// );

module top_module (
    input [1:0] sel,
    input [7:0] a,
    input [7:0] b,
    input [7:0] c,
    input [7:0] d,
    output [7:0] out  ); //

    wire [7:0] out_0, out_1;
    mux2 mux0 ( sel[0],    a,    b, out_0 );
    mux2 mux1 ( sel[0],    c,    d, out_1 );
    mux2 mux2 ( sel[1], out_0, out_1,  out );

endmodule

