// This three-input NAND gate doesn't work. Fix the bug(s).
//
// You must use the provided 5-input AND gate:
//
// module andgate ( output out, input a, input b, input c, input d, input e );

module top_module (input a, input b, input c, output out);//

    wire out_neg;
    andgate inst1 ( .a(a), .b(b), .c(c), .d(1'b1), .e(1'b1), .out(out_neg) );
    assign out = ~out_neg;
endmodule
