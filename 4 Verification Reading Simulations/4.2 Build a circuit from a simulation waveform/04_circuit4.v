// This is a combinational circuit. Read the simulation waveforms to determine what the circuit does, then implement it.
// https://hdlbits.01xz.net/wiki/sim/circuit4

module top_module (
    input a,
    input b,
    input c,
    input d,
    output q );//

    assign q = b | c;
endmodule
