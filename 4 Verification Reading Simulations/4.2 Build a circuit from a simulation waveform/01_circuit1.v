// This is a combinational circuit. Read the simulation waveforms to determine what the circuit does, then implement it.
// https://hdlbits.01xz.net/wiki/Sim/circuit1

module top_module (
    input a,
    input b,
    output q );//

    assign q = a & b;
endmodule
