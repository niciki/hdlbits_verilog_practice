// This is a sequential circuit. Read the simulation waveforms to determine what the circuit does, then implement it.
// https://hdlbits.01xz.net/wiki/Sim/circuit8

module top_module (
    input clock,
    input a,
    output p,
    output q );

    assign p = (clock) ? a : p;
    always @(negedge clock) begin
        q <= p;
    end
endmodule
