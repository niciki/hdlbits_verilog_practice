// This is a sequential circuit. The circuit consists of combinational logic and one bit of memory (i.e., one flip-flop). The output of the flip-flop has been made observable through the output state.
//
//     Read the simulation waveforms to determine what the circuit does, then implement it.
// https://hdlbits.01xz.net/wiki/Sim/circuit10

module top_module (
    input clk,
    input a,
    input b,
    output q,
    output state  );

    assign q = ^{a, b, state};
    always @(posedge clk) begin
        if (a & b) begin
            state <= 1'b1;
        end else if (~a & ~b) begin
            state <= 1'b0;
        end
    end
endmodule
