// This is a sequential circuit. Read the simulation waveforms to determine what the circuit does, then implement it.
// https://hdlbits.01xz.net/wiki/Sim/circuit9

module top_module (
    input clk,
    input a,
    output [2:0] q );

    always @(posedge clk) begin
        if (~a) begin
            q <= (q == 3'd6) ? 3'd0 : q + 3'd1;
        end else begin
            q <= 3'd4;
        end
    end
endmodule
